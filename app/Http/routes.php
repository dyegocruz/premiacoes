<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Login
Route::group(['middleware'=>'web'], function () {
	Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::get('/',     'HomeController@index');
});

//Admin
Route::group(['middleware'=>['web','auth'],'prefix' => 'admin','namespace'=>'Admin'], function () {	
	Route::get('/', ['as'=>'dashboard','uses'=>'DashboardController@index']);
	Route::resource('eventos','EventoController');
	Route::resource('categorias','CategoriaController');

	Route::post('indicados/search',['as'=>'admin.indicados.search','uses'=>'IndicadoController@index']);
	//Route::post('indicados/search?page={page}',['as'=>'admin.indicados.search','uses'=>'IndicadoController@index']);	
	Route::get('indicados/createmany',['as'=>'admin.indicados.createmany','uses'=>'IndicadoController@createmany']);		
	Route::post('indicados/createmanystore',['as'=>'admin.indicados.storemany','uses'=>'IndicadoController@createmanystore']);
	Route::resource('indicados','IndicadoController');	
});

//Guest
Route::get('/', ['as'=>'premiacoes','uses'=>'EventoController@index']);
Route::get('indicados/{slug}', ['as'=>'indicados','uses'=>'IndicadoController@indicadosPorEvento']);
Route::post('votacao/enviar', ['as'=>'votacaoenviar','uses'=>'VotacaoController@store']);
Route::get('votacao/view/{id}', ['as'=>'viewvotacao','uses'=>'VotacaoController@view']);