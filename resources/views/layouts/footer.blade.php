<div class="container logos-footer">
	<div class="row">				
		<div class="col s12 center-align">			
			<a href="http://modomeu.com">{{ Html::image('public/assets/images/logo-menu-horizontal-transparente.png', 'Logo Modo Meu') }}</a>
		</div>		
	</div>
	<div class="row">				
		<div class="col s12 center-align">
			<a href="http://benjaminstudio.com.br">{{ Html::image('public/assets/images/logo-benjamin-transparente.png', 'Logo Benjamin Studio') }}</a>
		</div>		
	</div>
</div>

<div class="footer-copyright">
	<div class="container center-align">
		© 2009- {{date('Y')}} Modo Meu / Benjamin Studio - ALL RIGHTS RESERVED
	</div>
</div>