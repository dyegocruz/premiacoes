<!doctype html>
<html>
<head>
    @include('layouts.head')
</head>
<body>
<main class="main-content">

    <header class="header">
        <h1>
        <a href="{{Config::get('app.url')}}" class="logo-top">
          Premiações Modo Meu
        </a>
        </h1>
    </header>

    <div class="container">    	

        <div class="content">

            @include('messages.errors')
            @include('messages.alerts')

            @yield('content')

        </div>        
    </div>

    <footer class="page-footer">        
        @include('layouts.footer')
    </footer>
</main>
</body>
</html>