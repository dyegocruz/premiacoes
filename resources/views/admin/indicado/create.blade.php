@extends('layouts.admin.default')

@section('content')

<div class="page-title">
	<span class="title">Form Indicado Create</span>	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-body">
				{!! Form::open([
				    'route' => 'admin.indicados.store'
				]) !!}
				<div class="sub-title">Nome</div>
				<div>					
					{!! Form::text('indicado_nome', null, ['class' => 'form-control']) !!}
				</div>

				<div class="sub-title">Indicado Por</div>
				<div>					
					{!! Form::text('indicado_por', null, ['class' => 'form-control']) !!}
				</div>

				<div class="sub-title">Evento</div>
				<div>					
					{{ Form::select('evento_id', $eventos, null, array('class' => 'form-control')) }}
				</div>

				<div class="sub-title">Categoria</div>
				<div>					
					{{ Form::select('categoria_id', $categorias, null, array('class' => 'form-control')) }}
				</div>

				<div class="checkbox">
	              	<div class="checkbox3 checkbox-danger checkbox-inline checkbox-check  checkbox-circle checkbox-light">
	                	{!! Form::checkbox('indicado_vencedor_oficial',1,null, ['id'=>'indicado_vencedor_oficial']) !!}
	                	<label for="indicado_vencedor_oficial">Vencedor Oficial</label>
	              	</div>
              	</div>

              	<div class="checkbox">
	              	<div class="checkbox3 checkbox-danger checkbox-inline checkbox-check  checkbox-circle checkbox-light">
	                	{!! Form::checkbox('indicado_vencedor_blog',1,null, ['id'=>'indicado_vencedor_blog']) !!}
	                	<label for="indicado_vencedor_blog">Vencedor Blog</label>
	              	</div>
              	</div>

				{!! Form::submit('Gravar Indicado', ['class' => 'btn btn-success']) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection