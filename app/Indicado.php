<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicado extends Model
{
    protected $table = 'indicados';
    protected $primaryKey = 'indicado_id';
    protected $fillable = ['indicado_nome','indicado_por','evento_id','categoria_id','indicado_vencedor_oficial','indicado_vencedor_blog'];

    protected function findIndicadosByEventoId($evento_id){
    	$result = $this::join('categorias', 'indicados.categoria_id', '=', 'categorias.categoria_id')    
            			->where('evento_id','=',$evento_id)       
            			->get();
        return $result;
    }

    protected function findIndicadosByIndicadoIds($indicados_in){    	
    	$result = $this::join('categorias', 'indicados.categoria_id', '=', 'categorias.categoria_id')    
    					->whereRaw('indicado_id in ('.$indicados_in.')')
            			->get();
        return $result;
    }

}
