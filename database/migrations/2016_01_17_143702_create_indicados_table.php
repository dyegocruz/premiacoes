<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicados', function (Blueprint $table) {
            $table->increments('indicado_id');
            $table->string('indicado_nome');
            $table->string('indicado_por');
            $table->integer('categoria_id');
            $table->integer('evento_id');
            $table->integer('indicado_vencedor_oficial');
            $table->integer('indicado_vencedor_blog');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('indicados');
    }
}
