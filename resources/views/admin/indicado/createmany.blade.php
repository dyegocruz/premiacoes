@extends('layouts.admin.default')

@section('content')

<div class="page-title">
	<span class="title">Form vários Indicados por evento e categoria</span>	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-body">
				{!! Form::open([
				    'route' => 'admin.indicados.storemany'
				]) !!}
				<div class="row">
					<div class="col-xs-6">
						<div class="sub-title">Evento</div>
						<div>					
							{{ Form::select('evento_id', $eventos, null, array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="col-xs-6">
						<div class="sub-title">Categoria</div>
						<div>
							{{ Form::select('categoria_id', $categorias, null, array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="input_fields_wrap">
							<div class="row">
							    <div class="col-xs-6">
							    	<div class="sub-title">Nome</div>
									<div>
										{!! Form::text('indicado_nome[]', null, ['class' => 'form-control']) !!}
									</div>
							    </div>
							    <div class="col-xs-6">
							    	<div class="sub-title">Indicado por</div>
									<div>
										{!! Form::text('indicado_por[]', null, ['class' => 'form-control']) !!}
									</div>
						    	</div>
						    </div>
						</div>						    
						    <button type="button" class="btn btn-info add_field_button">Adicionar Indicado</button>
					</div>
				</div>								

				{!! Form::submit('Gravar Indicados', ['class' => 'btn btn-success']) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection