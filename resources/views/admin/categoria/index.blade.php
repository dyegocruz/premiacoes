@extends('layouts.admin.default')

@section('content')

    <div class="page-title">
        <span class="title">Categorias</span>
        <div class="description">Categorias dos eventos.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">                
                <div class="card-body">
                	<a href="{{route('admin.categorias.create')}}">
                		<button type="button" class="btn btn-success">Adicionar</button>
                	</a>
                    <table class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th> 
								<th>Nome</th>								
								<th>Ação</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th> 
								<th>Nome</th> 								
								<th>Ação</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($categorias as $categoria)
								<tr>
									<th scope="row">{{$categoria->categoria_id}}</th>
									<td>{{$categoria->categoria_nome}}</td>															
									<td>
										<a href="{{route('admin.categorias.edit',['id'=>$categoria->categoria_id])}}"><button type="button" class="btn btn-info">Editar</button></a>
                                        <!--{!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['admin.categorias.destroy', $categoria->id]
                                        ]) !!}
                                            {!! Form::submit('Excluir', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}-->
									</td>				
								</tr> 
							@endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$categorias->render()}}
                    </div>              
                </div>
            </div>
        </div>
    </div>

@endsection