@extends('layouts.default')

@section('content')

<form name="indicados" id="indicados" method="POST" action="<?php echo route('votacaoenviar') ?>" class="col s12">
	<input type="hidden" name="evento_id" value="<?php echo $evento->evento_id; ?>" />	
	<div class="row">

		<div class="col s12">
			<div class="icon-block">	            
	            <h2 class="center">{{ $meta['title'] }}</h2>

	            <div class="col s12"> 
	            	<p class="light style-font" >Para você, quem dever ganhar? O blog Modo Meu e parceiros criaram a votação popular do {{$evento->evento_nome}}. Você poderá escolher entre os indicados quem merece ganhar o prêmio, receber a sua lista de votos por e-mail e poder comparar os seus erros e acertos de acordo com o resultado da premiação, que orcorrerá no dia {{date('d/m/Y',strtotime($evento->evento_data))}}.</p>
	            	<p class="light style-font">Faça a sua aposta do ano!</p>
	            	<p class="light style-font">P.S.: Não é obrigatório escolher indicados de todas as categorias, basta escolher as que você conhecer ou se interessar.</p>
	        	</div>
	        </div>
		</div>

	    <div class="row">
		    <div class="col s12 m12 l6">
				<div class="input-field">
		          <i class="material-icons prefix">account_circle</i>
		          <input id="icon_prefix" placeholder="Nome" type="text" name="votacao_usuario" class="validate" value="{{ old('votacao_usuario') }}">
		          <label for="icon_prefix"></label>
		        </div>
		    </div>
		    <div class="col s12 m12 l6">
				<div class="input-field">
		          <i class="material-icons prefix">email</i>
		          <input id="icon_prefix_mail" placeholder="E-mail" type="email" name="votacao_email" class="validate" value="{{ old('votacao_email') }}">
		          <label for="icon_prefix_mail" data-error="E-mail inválido."></label>

		        </div>
		    </div>		    
		</div>

		<div class="row">

			@foreach ($categorias as $cat_id => $cat_nome)
				<div class="col s12 m12 l6">
				    <div class="col s12">
				    	<h3>{{ $cat_nome }}</h3>
					</div>
					
				    @foreach ($indicados_cat[$cat_id] as $indicado)
						<div class="chopped box col s12">
							<label>
							    <input type="radio" 
							    		name="cat[{{$cat_id}}]" 
							    		id="indicado_{{$indicado->indicado_id}}" 
							    		value="{{$indicado->indicado_id}}" 										
										
										@if(Request::old('cat.'.$cat_id) == $indicado->indicado_id) checked @endif
							    		/>
							    <label for="indicado_{{$indicado->indicado_id}}">{{$indicado->indicado_nome}} @if ($indicado->indicado_por) - {{$indicado->indicado_por}} @endif</label>
					    	</label>
						</div>
					@endforeach
				</div>

			@endforeach
		</div>
		<div class="row">
			<div class="col s12 m12 l7">
				<p>
					<input type="checkbox" name="newsletter" id="newsletter" value="1" checked />
					<label for="newsletter">Aceito receber e-mails com novidades do blog, podcast e canal do youtube do Modo Meu.</label>
			    </p>
			</div>
			<div class="col s12 m12 l5">
				<p class="center">
					<button class="btn waves-effect waves-light center" id="button_send" type="submit" name="action">Enviar
				    	<i class="material-icons right">send</i>
				  	</button>
				</p>
			</div>
		</div>
	</div>
</form>

@stop