<?php

/**
* Recebe o valor 0 ou 1 e retorna Sim ou Não de acordo com o valor informado
*/
function returnStatusSimNao($value){
	if(isset($value) and $value == 1)
		return 'Sim';
	
	return 'Não';
}