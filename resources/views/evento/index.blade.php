@extends('layouts.default')

@section('content')

<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s12 m12 l12">
			    <div class="col s12">
			    	<h3>Prêmios</h3>
				</div>
				<div class="collection">					
					@foreach($eventos as $evento)						
						<a href="{{route('indicados',['id'=>$evento->evento_slug])}}" class="collection-item ">
							{{$evento->evento_nome}}
						</a>
					@endforeach					
				</div>
			</div>
		</div>
	</div>
</div>
@stop