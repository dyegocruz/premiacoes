@if (count($errors) > 0)
	<div class="row">
	    <div class="alert alert-danger">
	        <ul class="col s12 m12 l4">
	            @foreach ($errors->all() as $error)           
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	</div>
@endif