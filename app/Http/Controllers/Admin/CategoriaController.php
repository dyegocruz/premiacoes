<?php

namespace App\Http\Controllers\Admin;

use App\Categoria;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    public function index()
    {        
        $meta = [
            'title' => 'Categorias'
        ];

        $categorias = Categoria::paginate(10);
        return view('admin.categoria.index')->with(compact('categorias','meta'));
    }

    public function create(){        
        $meta = [
            'title' => 'Criar Categoria'
        ];

        return view('admin.categoria.create')->with(compact('meta'));
    }

    public function edit($id){

        $meta = [
            'title' => 'Editar Evento'
        ];

        $categoria = Categoria::findOrFail($id);
     
        return view('admin.categoria.edit')->with(compact('categoria','meta'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'categoria_nome' => 'required'
        ]);

        $inputs = $request->all();

        Categoria::create($inputs);
        $request->session()->flash('alert-success','Categoria criada com sucesso!');

        return redirect()->back();
    }

    public function update($id, Request $request){

        $categoria = Categoria::findOrFail($id);

        $this->validate($request, [
            'categoria_nome' => 'required'          
        ]);

        $inputs = $request->all();
        
        $categoria->fill($inputs)->save();

        $request->session()->flash('alert-success','Categoria atualizado com sucesso!');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $categoria = Evento::findOrFail($id);

        $categoria->delete();

        Session::flash('alert-success', 'Evento excluído com sucesso!');

        return redirect()->route('admin.eventos.index');
    }
}