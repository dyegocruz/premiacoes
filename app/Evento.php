<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'eventos';
    protected $primaryKey = 'evento_id';
    protected $fillable = ['evento_nome','evento_slug', 'evento_ano', 'evento_data','evento_encerrado'];
}
