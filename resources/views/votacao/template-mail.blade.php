<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$evento->evento_nome}} no Modo Meu</title>
	<style type="text/css">
		body {
			/*background: url('../images/Oscar_Modo_Meu_bg.png') repeat;*/
		}

		h2,h3 { font-family: 'Dosis', sans-serif; }
		h2 { color: #12aec0; }
		h3 { color: #3a4192; }

		.main-content .header { margin: 0 auto; width: 70%; }
		.main-content .header .logo-top { margin: 0; }
		.main-content .header .logo-top h1 { 
			margin: 0 auto; 
			/*background: url('../images/Oscar_Modo_Meu_logo_03.png') no-repeat top center;*/
			background-color: rgba(255, 255, 255, 0.9);	
			width: auto;
			height: 171px;
			text-indent: -9999px;
		}

		label { white-space:nowrap; }
		.wrappable { white-space:normal; }

		.content {
			width: 90%;
			margin: 0 auto;
			padding: 2% 1%;
			background: rgba(255, 255, 255, 0.95);
		}

		.footer { background: #3a4192; }

		/* Modelo para template de e-mail */
		.template-view h2 { font-size: 20pt; }
		.template-view h3 { font-size: 18pt; }
		.template-view p { 
			font-family: 'Dosis', sans-serif;
			font-size: 14pt;
		    color: #898989;    
		    margin: 0;  
		}

		/* Modelo para template de alertas e mensagens */
		.alert {
			color: #cc0000;
			font-weight: bold;
		}

		.alert ul li {
			list-style-type: circle;
		    width: 30%;
		    margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="template-view">

		<h2>Estas foram as suas escolhas do {{$evento->evento_nome}} no Modo Meu</h2>

		<div class="row">
			@foreach($indicados as $indicado)
				<div class="box col s12 m6 l6">
				    <div class="col s12">
				    	<h3>{{$indicado['categoria_nome']}}</h3>
					</div>
					<div class="chopped box col s12">
					  <p>{{$indicado['indicado_nome']}} @if ($indicado['indicado_por']) - {{$indicado['indicado_por']}} @endif</p>	
					</div>
				</div>
			@endforeach
		</div>
	</div>
</body>
</html>