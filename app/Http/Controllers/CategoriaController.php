<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Controllers\Controller;

class CategoriaController extends Controller
{
    /**
     * Show the profile for the given Evento.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        
        $meta = [
            'title' => 'Categorias'
        ];

        $categorias = Categoria::all();        
        return view('categoria.index')->with(compact('categorias','meta'));
    }
}