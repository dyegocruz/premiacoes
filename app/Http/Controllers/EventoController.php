<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Indicado;
use App\Http\Controllers\Controller;

class EventoController extends Controller
{
    /**
     * Show the profile for the given Evento.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {        
        $meta = [
            'title' => 'Eventos'
        ];

        $eventos = Evento::orderby('evento_data','DESC')->get();

        return view('evento.index')->with(compact('eventos','meta'));
    }

    public function edit(){

    }
}