<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votacao extends Model
{
    protected $table = 'votacoes';
    protected $primaryKey = 'votacao_id';

    protected $fillable = ['votacao_usuario', 'votacao_email', 'evento_id','indicados_serial','newsletter'];

    protected function checkVotoUsuario($evento_id,$email){    	
        $result = $this::where('evento_id','=',$evento_id)
			            ->where('votacao_email','=',trim($email))
			            ->get();
                
        if(count($result) > 0){
            return $result[0];    
        }else{
            return false;
        }
        
    }    

}
