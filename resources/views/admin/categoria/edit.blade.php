@extends('layouts.admin.default')

@section('content')

<div class="page-title">
	<span class="title">Form Categoria</span>	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-body">
				
				{{ Form::model($categoria, array('route' => array('admin.categorias.update', $categoria->categoria_id), 'method' => 'PUT')) }}
				
					<div class="sub-title">Nome</div>
					<div>					
						{!! Form::text('categoria_nome', null, ['class' => 'form-control']) !!}
					</div>					

				{!! Form::submit('Atualizar Categoria', ['class' => 'btn btn-success']) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection