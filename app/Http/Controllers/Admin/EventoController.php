<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Evento;
use App\Http\Controllers\Controller;
use App\Http\Redirect;
use Illuminate\Http\Request;

class EventoController extends Controller
{
    /**
     * Show the profile for the given Evento.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {        
        $meta = [
            'title' => 'Eventos'
        ];

        $eventos = Evento::paginate(10);
        return view('admin.evento.index')->with(compact('eventos','meta'));        
    }

    public function create(){        
        $meta = [
            'title' => 'Criar Evento'
        ];

        return view('admin.evento.create')->with(compact('meta'));
    }

    public function edit($id){

        $meta = [
            'title' => 'Editar Evento'
        ];

        $evento = Evento::findOrFail($id);
     
        return view('admin.evento.edit')->with(compact('evento','meta'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'evento_nome' => 'required',
            'evento_ano' => 'required',
            'evento_data' => 'required'            
        ]);

        $inputs = $request->all();

        if(!isset($inputs['evento_slug'])){
            $inputs['evento_slug'] = str_slug($inputs['evento_nome'], "-");
        }

        Evento::create($inputs);
        $request->session()->flash('alert-success','Evento criado com sucesso!');

        return redirect()->back();
    }

    public function update($id, Request $request){

        $evento = Evento::findOrFail($id);

        $this->validate($request, [
            'evento_nome' => 'required',
            'evento_ano' => 'required',
            'evento_data' => 'required'            
        ]);        

        $inputs = $request->all();        

        if(!isset($inputs['evento_slug'])){
            $inputs['evento_slug'] = str_slug($inputs['evento_nome'], "-");
        }
        
        $evento->fill($inputs)->save();

        $request->session()->flash('alert-success','Evento atualizado com sucesso!');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $evento = Evento::findOrFail($id);

        $evento->delete();

        Session::flash('alert-success', 'Evento excluído com sucesso!');

        return redirect()->route('admin.eventos.index');
    }
    
}