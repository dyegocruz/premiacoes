@extends('layouts.admin.default')

@section('content')

<div class="page-title">
	<span class="title">Form Categoria Create</span>	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-body">
				{!! Form::open([
				    'route' => 'admin.categorias.store'
				]) !!}
				<div class="sub-title">Nome</div>
				<div>					
					{!! Form::text('categoria_nome', null, ['class' => 'form-control']) !!}
				</div>						

				{!! Form::submit('Gravar Categoria', ['class' => 'btn btn-success']) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection