<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

@if(isset($meta['title']))
    <title>{{ $meta['title']}} - Permiações Modo Meu</title>
@else
    <title>Premiações Modo Meu</title>
@endif

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25540043-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- /Google Analytics -->

<meta name="robots" content="noodp,noydir"/>
<meta property="fb:admins" content="dyegocruz,marynfernandes" />
<meta property="fb:app_id" content="515005851873093" />

<link rel="shortcut icon" href="http://modomeu.com/wp-content/themes/modo-meu-theme-3.0/modo-meu.ico" type="image/x-icon">
<link rel="icon" href="http://modomeu.com/wp-content/themes/modo-meu-theme-3.0/modo-meu.ico" sizes="32x32">
<link href='http://fonts.googleapis.com/css?family=Dosis:600' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="{!! asset('public/assets/css/materialize.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
<link href="{!! asset('public/assets/css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="{!! asset('public/assets/js/jquery-1.12.0.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('public/assets/js/materialize.min.js') !!}"></script>

<link rel="canonical" href="{{Request::url()}}" />

<!-- OG FACEBOOK -->
<meta property="og:locale" content="pt_BR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$meta['title']}} - Modo Meu"/>
<meta property="og:description" content="Escolha também quem você acha que vai levar o Oscar 2016 em cada categoria e desafie seus amigos." />
<meta property="og:url" content="{{Request::url()}}" />
<meta property="og:site_name" content="Premiações - Modo Meu" />
<meta property="og:image" content="{{Config::get('app.url')}}/public/assets/images/Modo_Meu_preimiacoes.png" />

<!-- OG FACEBOOK -->