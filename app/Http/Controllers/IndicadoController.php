<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Indicado;
use App\Http\Controllers\Controller;

class IndicadoController extends Controller
{    

    public function indicadosPorEvento($slug){
     
        $evento = Evento::where('evento_slug',$slug)->first();
        
        if(count($evento) == 0){
            $meta = ['title' => 'Indicados não encontrados - Preiações Modo Meu'];
            return view('indicado.indicados_nao_encontrados')->with(compact('meta'));
        }

        $meta = [
            'title' => 'Indicados do '.$evento->evento_nome            
        ];        

        $indicados = Indicado::findIndicadosByEventoId($evento->evento_id);

        foreach($indicados as $indicado){
            $categorias[$indicado->categoria_id] = $indicado->categoria_nome; 
            $indicados_cat[$indicado->categoria_id][$indicado->indicado_id] = $indicado;
        }
        
        //if(time() >= strtotime($evento->dt_hora_encerramento)){
        if($evento->evento_encerrado == 1){
            $meta['title'] = 'Vencedores do '.$evento->evento_nome;
            return view('indicado.indicadosResultado')->with(compact('meta','evento','categorias','indicados_cat'));
        }

        return view('indicado.indicadosPorEvento')->with(compact('meta','evento','categorias','indicados_cat'));

    }
}