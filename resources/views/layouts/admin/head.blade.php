<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<title>Modo Meu Premiações | Admin</title>

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

<!-- CSS Libs -->
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/bootstrap.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/font-awesome.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/animate.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/bootstrap-switch.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/checkbox3.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/jquery.dataTables.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/dataTables.bootstrap.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/select2.min.css') !!}">

<!-- CSS App -->
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/flat-admin/style.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('public/assets/css/flat-admin/themes/flat-blue.css') !!}">