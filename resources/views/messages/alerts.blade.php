@if(Session::has('alert-danger') or Session::has('alert-warning') or Session::has('alert-success') or Session::has('alert-info'))
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
		@if(Session::has('alert-' . $msg))
			<div class="row">
				<div class="alert alert-{{ $msg }}">
					<ul class="col s12 m12 l4">				
						<li>{{ Session::get('alert-' . $msg) }}</li>					
					</ul>
				</div>
			<div>
		@endif
	@endforeach
@endif