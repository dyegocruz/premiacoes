<?php

namespace App\Http\Controllers\Admin;

use App\Indicado;
use App\Categoria;
use App\Evento;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndicadoController extends Controller
{

	public function index(Request $request)
    {        
       
        $meta = [
            'title' => 'Indicados'
        ];

        $inputs = $request->all();        
        if($request->get('search')){                        
            $indicados = Indicado::where(function($q) use ($request) {
                $q->orWhere("indicado_nome", "LIKE", "%{$request->get('search')}%");
                $q->orWhere('categoria_id', $request->get('categoria_id'));
                $q->orWhere('evento_id', $request->get('evento_id'));
            })->latest()->paginate(6);            
        }else{            
            $indicados = Indicado::paginate(10);
        }
        
        $categoria = new Categoria();
        $evento = new Evento();
        
        return view('admin.indicado.index')->with(compact('indicados','categoria','evento','meta'));
    }

    public function create(){
        $meta = [
            'title' => 'Criar Indicado'
        ];

        $categorias = Categoria::lists('categoria_nome', 'categoria_id')->prepend('Categorias');
        $eventos = Evento::lists('evento_nome', 'evento_id')->prepend('Eventos');

        return view('admin.indicado.create')->with(compact('meta','categorias','eventos'));
    }

    public function createmany(){
        $meta = [
            'title' => 'Criar vários Indicados por evento e categoria'
        ];

        $categorias = Categoria::lists('categoria_nome', 'categoria_id')->prepend('Categorias');
        $eventos = Evento::lists('evento_nome', 'evento_id')->prepend('Eventos');

        return view('admin.indicado.createmany')->with(compact('meta','categorias','eventos'));
    }

    public function createmanystore(Request $request){
        
        $inputs = $request->all();

        if($inputs['evento_id'] == 0){
            $request->session()->flash('alert-danger', 'Favor escolher um evento.');
            return redirect()->back();
        }

        if($inputs['categoria_id'] == 0){
            $request->session()->flash('alert-danger', 'Favor escolher uma categoria.');
            return redirect()->back();
        }

        if($inputs['indicado_nome'][0] == ''){
            $request->session()->flash('alert-danger', 'Favor incluir um ou mais indicados.');
            return redirect()->back();   
        }else{
            $indicados_por = $inputs['indicado_por'];
            foreach ($inputs['indicado_nome'] as $key => $indicado_nome) {
                    
                if($indicado_nome != ''){
                    $indicado = new Indicado;
                    $indicado->evento_id = $inputs['evento_id'];
                    $indicado->categoria_id = $inputs['categoria_id'];
                    $indicado->indicado_nome = $indicado_nome;
                    $indicado->indicado_por = $indicados_por[$key];
                    $indicado->save();
                }
                
            }
        }
        
        $request->session()->flash('alert-success','Indicados criados com sucesso!');

        return redirect()->back();
    }

    public function edit($id){

        $meta = [
            'title' => 'Editar Indicado'
        ];

        $categorias = Categoria::lists('categoria_nome', 'categoria_id')->prepend('Categorias');
        $eventos = Evento::lists('evento_nome', 'evento_id')->prepend('Eventos');

        $indicado = Indicado::findOrFail($id);
     
        return view('admin.indicado.edit')->with(compact('indicado','categorias','eventos','meta'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'indicado_nome' => 'required',
            'evento_id' => 'exists:eventos,evento_id',
            'categoria_id' => 'exists:categorias,categoria_id'
        ]);

        $inputs = $request->all();        

        Indicado::create($inputs);
        $request->session()->flash('alert-success','Indicado criado com sucesso!');

        return redirect()->back();
    }

    public function update($id, Request $request){

        $indicado = Indicado::findOrFail($id);

        $this->validate($request, [
            'indicado_nome' => 'required',
            'evento_id' => 'exists:eventos,evento_id',
            'categoria_id' => 'exists:categorias,categoria_id'
        ]);

        $inputs = $request->all();
        
        $indicado->fill($inputs)->save();

        $request->session()->flash('alert-success','Indicado atualizado com sucesso!');

        return redirect()->back();
    }

    // public function search(Request $request){
    //     echo '>oi';
    //     exit;
    // }

}