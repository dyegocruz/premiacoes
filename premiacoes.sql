-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 17-Ago-2016 às 11:51
-- Versão do servidor: 5.5.48-37.8
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `modom657_premiacoes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `categoria_id` int(11) NOT NULL,
  `categoria_nome` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`categoria_id`, `categoria_nome`, `created_at`, `updated_at`) VALUES
(1, 'Melhor Filme', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Melhor Diretor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Melhor Atriz', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Melhor Ator', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Melhor Ator Coadjuvante', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Melhor Atriz Coadjuvante', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Melhor Roteiro Original', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Melhor Roteiro Adaptado', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Melhor Animação', '2016-08-12 12:18:45', '0000-00-00 00:00:00'),
(10, 'Melhor Documentário em Curta-Metragem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Melhor Documentário em Longa-Metragem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Melhor Longa Estrangeiro', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Melhor Curta-Metragem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Melhor Curta em Animação', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Melhor Canção Original', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Melhor Fotografia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Melhor Figurino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Melhor Maquiagem e Cabelo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Melhor Mixagem de Som', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Melhor Edição de Som', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Melhores Efeitos Visuais', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Melhor Design de Produção', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Melhor Edição', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Melhor Trilha Sonora', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Melhor série dramática', '2016-08-11 13:47:58', '2016-08-11 13:47:58'),
(26, 'Melhor atriz em série dramática', '2016-08-11 13:48:06', '2016-08-11 13:48:06'),
(27, 'Melhor ator em série dramática', '2016-08-11 13:48:16', '2016-08-11 13:48:16'),
(28, 'Melhor atriz coadjuvante em série dramática', '2016-08-11 13:49:23', '2016-08-11 13:49:23'),
(29, 'Melhor ator coadjuvante em série dramática', '2016-08-11 13:49:30', '2016-08-11 13:49:30'),
(30, 'Melhor atriz convidada em série dramática', '2016-08-11 13:49:36', '2016-08-11 13:49:36'),
(31, 'Melhor ator convidado em série dramática', '2016-08-11 13:49:44', '2016-08-11 13:49:44'),
(32, 'Melhor direção em série dramática', '2016-08-11 13:49:51', '2016-08-11 13:49:51'),
(33, 'Melhor roteiro em série dramática', '2016-08-11 13:49:59', '2016-08-11 13:49:59'),
(34, 'Melhor série cômica', '2016-08-11 13:50:06', '2016-08-11 13:50:06'),
(35, 'Melhor atriz em série cômica', '2016-08-11 13:50:12', '2016-08-11 13:50:12'),
(36, 'Melhor ator em série cômica', '2016-08-11 13:50:19', '2016-08-11 13:50:19'),
(37, 'Melhor atriz coadjuvante em série cômica', '2016-08-11 13:50:29', '2016-08-11 13:50:29'),
(38, 'Melhor ator coadjuvante em série cômica', '2016-08-11 13:50:36', '2016-08-11 13:50:36'),
(39, 'Melhor atriz convidada em série cômica', '2016-08-11 13:50:43', '2016-08-11 13:50:43'),
(40, 'Melhor ator convidado em série cômica', '2016-08-11 13:50:51', '2016-08-11 13:50:51'),
(41, 'Melhor direção série cômica', '2016-08-11 13:50:58', '2016-08-11 13:50:58'),
(42, 'Melhor roteiro em série cômica', '2016-08-11 13:51:04', '2016-08-11 13:51:04'),
(43, 'Melhor minissérie', '2016-08-11 13:51:11', '2016-08-11 13:51:11'),
(44, 'Melhor filme feito para TV', '2016-08-11 13:51:18', '2016-08-11 13:51:18'),
(45, 'Melhor atriz em minissérie ou filme feito para TV', '2016-08-11 13:51:24', '2016-08-11 13:51:24'),
(46, 'Melhor ator em minissérie ou filme feito para TV', '2016-08-11 13:51:31', '2016-08-11 13:51:31'),
(47, 'Melhor atriz coadjuvante em minissérie ou filme feito para TV', '2016-08-11 13:51:38', '2016-08-11 13:51:38'),
(48, 'Melhor ator coadjuvante em minissérie ou filme feito para TV', '2016-08-11 13:51:45', '2016-08-11 13:51:45'),
(49, 'Melhor roteiro em minissérie ou filme feito para a TV', '2016-08-11 13:51:51', '2016-08-11 13:51:51'),
(50, 'Melhor direção em minissérie ou filme feito para a TV', '2016-08-11 13:51:59', '2016-08-11 13:51:59'),
(51, 'Melhor Dublagem', '2016-08-12 12:19:36', '2016-08-12 12:19:36'),
(52, 'Melhor talk show e variedades', '2016-08-12 12:20:51', '2016-08-12 12:20:51'),
(53, 'Melhor programa de esquete e variedades', '2016-08-12 12:20:57', '2016-08-12 12:20:57'),
(54, 'Melhor especial de variedades', '2016-08-12 12:21:03', '2016-08-12 12:21:03'),
(55, 'Melhor reality show ou programa de competição', '2016-08-12 12:21:09', '2016-08-12 12:21:09'),
(56, 'Melhor apresentador de reality show', '2016-08-12 12:21:13', '2016-08-12 12:21:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos`
--

CREATE TABLE IF NOT EXISTS `eventos` (
  `evento_id` int(11) NOT NULL,
  `evento_nome` varchar(100) NOT NULL,
  `evento_slug` varchar(255) NOT NULL,
  `evento_ano` int(4) NOT NULL,
  `evento_data` date DEFAULT NULL,
  `evento_encerrado` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `eventos`
--

INSERT INTO `eventos` (`evento_id`, `evento_nome`, `evento_slug`, `evento_ano`, `evento_data`, `evento_encerrado`, `created_at`, `updated_at`) VALUES
(1, 'Oscar 2016', 'oscar-2016', 2016, '2016-02-28', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Emmy 2016', 'emmy-2016', 2016, '2016-09-18', 0, '2016-08-11 13:47:40', '2016-08-11 13:47:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `indicados`
--

CREATE TABLE IF NOT EXISTS `indicados` (
  `indicado_id` int(11) NOT NULL,
  `indicado_nome` varchar(255) NOT NULL,
  `indicado_por` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  `evento_id` int(11) NOT NULL,
  `indicado_vencedor_oficial` int(1) NOT NULL DEFAULT '0',
  `indicado_vencedor_blog` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `indicados`
--

INSERT INTO `indicados` (`indicado_id`, `indicado_nome`, `indicado_por`, `categoria_id`, `evento_id`, `indicado_vencedor_oficial`, `indicado_vencedor_blog`, `created_at`, `updated_at`) VALUES
(1, 'Mad Max - Estrada da Fúria', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'O Regresso', NULL, 1, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'O Quarto de Jack', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Spotlight - Segredos Revelados', NULL, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'A Grande Aposta', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Ponte dos Espiões', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Brooklyn', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Perdido em Marte', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Alejandro G. Iñárritu', 'O Regresso', 2, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Tom McCarthy - Spotlight', 'Segredos Revelados', 2, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Adam McKay', 'A Grande Aposta', 2, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'George Miller', 'Mad Max: Estrada da Fúria', 2, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Lenny Abrahamson', 'O Quarto de Jack', 2, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Cate Blanchett', 'Carol', 3, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Brie Larson', 'O Quarto de Jack', 3, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Saoirse Ronan', 'Brooklyn', 3, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Charlotte Rampling', '45 Anos', 3, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Jennifer Lawrence', 'Joy - o Nome do Sucesso', 3, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Bryan Cranston', 'Trumbo', 4, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Leonardo DiCaprio', 'O Regresso', 4, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Michael Fassbender', 'Steve Jobs', 4, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Eddie Redmayne', 'A Garota Dinamarquesa', 4, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Matt Damon', 'Perdido em Marte', 4, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Christian Bale', 'A Grande Aposta', 5, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Tom Hardy', 'O Regresso', 5, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Mark Ruffalo', 'Spotlight - Segredos Revelados', 5, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Mark Rylance', 'Ponte dos Espiões', 5, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Sylvester Stallone', 'Creed - Nascido para Lutar', 5, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Jennifer Jason Leigh', 'Os 8 Odiados', 6, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Rooney Mara', 'Carol', 6, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Rachel McAdams', 'Spotlight - Segredos Revelados', 6, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Alicia Vikander', 'A Garota Dinamarquesa', 6, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Kate Winslet', 'Steve Jobs', 6, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Matt Charman', 'Ponte dos Espiões', 7, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Alex Garland', 'Ex Machina', 7, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Peter Docter, Meg LeFauve, Josh Cooley', 'Divertida Mente', 7, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Josh Singer, Tom McCarthy - Spotlight', 'Segredos Revelados', 7, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Jonathan Herman, Andrea Berloff', 'Straigh Outta Comptom', 7, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Charles Randolph, Adam McKay', 'A Grande Aposta', 8, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Nick Hornby', 'Brooklyn', 8, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Phyllis Nagy', 'Carol', 8, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Drew Goddard', 'Perdido em Marte', 8, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Emma Donoghue', 'O Quarto de Jack', 8, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Anomalisa', NULL, 9, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'Divertida Mente', NULL, 9, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Shaun, o Carneiro', NULL, 9, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'O Menino e o Mundo', NULL, 9, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'As Memórias de Marnie', NULL, 9, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Body Team 12', NULL, 10, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Chau, Beyond The Lines', NULL, 10, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Claude Lanzmann: Spectres Of The Shoah', NULL, 10, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'A Girl In The River: The Price Of Forgiveness', NULL, 10, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Last Day Of Freedom', NULL, 10, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Amy', NULL, 11, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Cartel Land', NULL, 11, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'O Peso do Silêncio', NULL, 11, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'What Happened, Miss Simone?', NULL, 11, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Winter on Fire: Ukraine''s Fight fo Freedom', NULL, 11, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Theeb - Jordânia', NULL, 12, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'A War - Dinamarca', NULL, 12, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Mustang - França', NULL, 12, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Saul Fia (Son Of Saul) - Hungria', NULL, 12, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Embrace Of The Serpent - Colombia', NULL, 12, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Ave Maria', NULL, 13, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Day One', NULL, 13, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Everything Will Be Okay (Alles Wird Gut)', NULL, 13, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Shok', NULL, 13, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Stutterer', NULL, 13, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Bear Story', NULL, 14, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Prologue', NULL, 14, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Os Heróis de Sanjay', NULL, 14, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'We Can''t Live Without Cosmos', NULL, 14, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'World of Tomorrow', NULL, 14, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, '"Earned It"', 'The Weeknd - Cinquenta Tons de Cinza', 15, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, '"Manta Ray"', 'J. Ralph & Anthony - Racing Extinction', 15, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, '"Simple Song #3"', 'Sumi Jo- Youth', 15, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, '"Writing''s On The Wall"', 'Sam Smith - 007 Contra Spectre', 15, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, '"Til It Happens To You"', 'Lady Gaga e Diane Warren- The Hunting Ground', 15, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Carol', NULL, 16, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Mad Max: Estrada da Fúria', NULL, 16, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'O Regresso', NULL, 16, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Sicario: Terra de Ninguém', NULL, 16, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Os 8 Odiados', NULL, 16, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'O Regresso', NULL, 17, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Carol', NULL, 17, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Cinderela', NULL, 17, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'A Garota Dinamarquesa', NULL, 17, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Mad Max: Estrada da Fúria', NULL, 17, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'The 100-Year-Old Man Who Climbed out The Window And Disappeared', NULL, 18, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Mad Max: Estrada da Fúria', NULL, 18, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'O Regresso', NULL, 18, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Ponte dos Espiões', NULL, 19, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Mad Max: Estrada da Fúria', NULL, 19, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Perdido em Marte', NULL, 19, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'O Regresso', NULL, 19, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Star Wars - O Despertar da Força', NULL, 19, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Sicario: Terra de Ninguém', NULL, 20, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Mad Max: Estrada da Fúria', NULL, 20, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'Perdido em Marte', NULL, 20, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'O Regresso', NULL, 20, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Star Wars - O Despertar da Força', NULL, 20, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Star Wars: O Despertar da Força', NULL, 21, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Mad Max: Estrada da Fúria', NULL, 21, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Perdido em Marte', NULL, 21, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'Ex Machina', NULL, 21, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'O Regresso', NULL, 21, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Ponte dos Espiões', NULL, 22, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'A Garota Dinamarquesa', NULL, 22, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Mad Max: Estrada da Fúria', NULL, 22, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'Perdido em Marte', NULL, 22, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'O Regresso', NULL, 22, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'A Grande Aposta', NULL, 23, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'Mad Max: Estrada da Fúria', NULL, 23, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'O Regresso', NULL, 23, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Spotlight - Segredos Revelados', NULL, 23, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'Star Wars: O Despertar da Força', NULL, 23, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'Carter Burwell', 'Carol', 24, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'Ennio Morricone', 'Os 8 Odiados', 24, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'Jóhann Jóhannsson', 'Sicario: Terra de Ninguém', 24, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'Thomas Newman', 'Ponte dos Espiões', 24, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'John Williams', 'Star Wars: O Despertar da Força', 24, 1, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'The Americans', '', 25, 2, 0, 0, '2016-08-11 13:53:12', '2016-08-11 13:53:12'),
(123, 'Better Call Saul', '', 25, 2, 0, 0, '2016-08-11 13:53:34', '2016-08-11 13:53:34'),
(124, 'Downton Abbey', '', 25, 2, 0, 0, '2016-08-11 13:53:51', '2016-08-11 13:53:51'),
(125, 'Game of Thrones', '', 25, 2, 0, 0, '2016-08-11 13:54:05', '2016-08-11 13:54:05'),
(126, 'Homeland', '', 25, 2, 0, 0, '2016-08-11 13:54:19', '2016-08-11 13:54:19'),
(127, 'House of Cards', '', 25, 2, 0, 0, '2016-08-11 13:54:37', '2016-08-11 13:54:37'),
(128, 'Mr. Robot ', '', 25, 2, 0, 0, '2016-08-11 13:54:53', '2016-08-11 13:54:53'),
(129, 'Claire Danes', 'Homeland', 26, 2, 0, 0, '2016-08-11 13:55:57', '2016-08-11 13:55:57'),
(130, 'Viola Davis', 'How To Get Away With Murder', 26, 2, 0, 0, '2016-08-11 13:56:27', '2016-08-11 13:56:27'),
(131, 'Taraji P. Henson', 'Empire', 26, 2, 0, 0, '2016-08-11 13:56:48', '2016-08-11 13:56:48'),
(132, 'Tatiana Maslany', 'Orphan Black', 26, 2, 0, 0, '2016-08-11 13:57:11', '2016-08-11 13:57:11'),
(133, 'Keri Russell', 'The Americans', 26, 2, 0, 0, '2016-08-11 13:57:33', '2016-08-11 13:57:33'),
(134, 'Robin Wright', 'House of Cards', 26, 2, 0, 0, '2016-08-11 13:58:02', '2016-08-11 13:58:02'),
(135, 'Kyle Chandler', 'Bloodline', 27, 2, 0, 0, '2016-08-11 13:58:41', '2016-08-11 13:58:41'),
(136, 'Rami Malek', 'Mr. Robot', 27, 2, 0, 0, '2016-08-11 13:59:01', '2016-08-11 13:59:01'),
(137, 'Bob Odenkirk', 'Better Call Saul', 27, 2, 0, 0, '2016-08-11 13:59:29', '2016-08-11 13:59:29'),
(138, 'Matthew Rhys', 'The Americans', 27, 2, 0, 0, '2016-08-11 14:00:07', '2016-08-11 14:00:07'),
(139, 'Liev Schreiber', 'Ray Donovan', 27, 2, 0, 0, '2016-08-11 14:00:30', '2016-08-11 14:00:30'),
(140, 'Kevin Spacey', 'House of Cards', 27, 2, 0, 0, '2016-08-11 14:00:52', '2016-08-11 14:00:52'),
(141, 'Maura Tierney', 'The Affair', 28, 2, 0, 0, '2016-08-11 14:01:22', '2016-08-11 14:01:22'),
(142, 'Maggie Smith', 'Downton Abbey', 28, 2, 0, 0, '2016-08-11 14:01:46', '2016-08-11 14:01:46'),
(143, 'Lena Headey', 'Game of Thrones', 28, 2, 0, 0, '2016-08-11 14:02:04', '2016-08-11 14:02:04'),
(144, 'Emilia Clarke', 'Game of Thrones', 28, 2, 0, 0, '2016-08-11 14:02:38', '2016-08-11 14:02:38'),
(145, 'Maisie Williams', 'Game of Thrones', 28, 2, 0, 0, '2016-08-11 14:02:55', '2016-08-11 14:02:55'),
(146, 'Constance Zimmer', 'UnREAL', 28, 2, 0, 0, '2016-08-11 14:03:17', '2016-08-11 14:03:17'),
(147, 'Jonathan Banks', 'Better Call Saul', 29, 2, 0, 0, '2016-08-11 14:27:16', '2016-08-11 14:27:16'),
(148, 'Ben Mendelsohn', 'Bloodline', 29, 2, 0, 0, '2016-08-11 14:27:33', '2016-08-11 14:27:33'),
(149, 'Peter Dinklage', 'Game of Thrones', 29, 2, 0, 0, '2016-08-11 14:27:49', '2016-08-11 14:27:49'),
(150, 'Kit Harington', 'Game of Thrones', 29, 2, 0, 0, '2016-08-11 14:28:11', '2016-08-11 14:28:11'),
(151, 'Michael Kelly', 'House of Cards', 29, 2, 0, 0, '2016-08-11 14:29:18', '2016-08-11 14:29:18'),
(152, 'Jon Voight', 'Ray Donovan', 29, 2, 0, 0, '2016-08-11 14:29:39', '2016-08-11 14:29:39'),
(153, 'Margo Martindale', 'The Americans', 30, 2, 0, 0, '2016-08-11 14:30:05', '2016-08-11 14:30:05'),
(154, 'Carrie Preston', 'The Good Wife', 30, 2, 0, 0, '2016-08-11 14:30:25', '2016-08-11 14:30:25'),
(155, 'Laurie Metcalf', 'Horace And Pete', 30, 2, 0, 0, '2016-08-11 14:30:58', '2016-08-11 14:30:58'),
(156, 'Ellen Burstyn', 'House of Cards', 30, 2, 0, 0, '2016-08-11 14:31:22', '2016-08-11 14:31:22'),
(157, 'Molly Parker', 'House of Cards', 30, 2, 0, 0, '2016-08-11 14:31:40', '2016-08-11 14:31:40'),
(158, 'Allison Janney', 'Masters of Sex', 30, 2, 0, 0, '2016-08-11 14:32:06', '2016-08-11 14:32:06'),
(159, 'Max von Sydow', 'Game of Thrones', 31, 2, 0, 0, '2016-08-12 11:35:52', '2016-08-12 11:35:52'),
(160, 'Michael J. Fox', 'The Good Wife', 31, 2, 0, 0, '2016-08-12 11:35:52', '2016-08-12 11:35:52'),
(161, 'Reg E. Cathey', 'House of Cards', 31, 2, 0, 0, '2016-08-12 11:35:52', '2016-08-12 11:35:52'),
(162, 'Mahershala Ali', 'House of Cards', 31, 2, 0, 0, '2016-08-12 11:35:52', '2016-08-12 11:35:52'),
(163, 'Paul Sparks', 'House of Cards', 31, 2, 0, 0, '2016-08-12 11:35:52', '2016-08-12 11:35:52'),
(164, 'Hank Azaria', 'Ray Donovan', 31, 2, 0, 0, '2016-08-12 11:35:52', '2016-08-12 11:35:52'),
(165, 'Michael Engler por Episódio 9', 'Downton Abbey', 32, 2, 0, 0, '2016-08-12 11:37:40', '2016-08-12 11:37:40'),
(166, 'Miguel Sapochnik por "Battle Of The Bastards"', 'Game of Thrones', 32, 2, 0, 0, '2016-08-12 11:37:41', '2016-08-12 11:37:41'),
(167, 'Jack Bender por "The Door"', 'Game of Thrones', 32, 2, 0, 0, '2016-08-12 11:37:41', '2016-08-12 11:37:41'),
(168, 'Lesli Linka Glatter por "The Tradition Of Hospitality"', 'Homeland', 32, 2, 0, 0, '2016-08-12 11:37:41', '2016-08-12 11:37:41'),
(169, 'Steven Soderbergh por "This is All We Are"', 'The Knick', 32, 2, 0, 0, '2016-08-12 11:37:41', '2016-08-12 11:37:41'),
(170, 'David Hollander por "Exsuscito"', 'Ray Donovan', 32, 2, 0, 0, '2016-08-12 11:37:41', '2016-08-12 11:37:41'),
(171, 'Joel Fields e Joe Weisberg por "Persona Non Grata"', 'The Americans', 33, 2, 0, 0, '2016-08-12 11:39:10', '2016-08-12 11:39:10'),
(172, 'Julian Fellowes por Episódio 8', 'Downton Abbey', 33, 2, 0, 0, '2016-08-12 11:39:10', '2016-08-12 11:39:10'),
(173, 'David Beniof e D.B. Weiss  por  "Battle Of The Bastards"', 'Game of Thrones', 33, 2, 0, 0, '2016-08-12 11:39:10', '2016-08-12 11:39:10'),
(174, 'Robert King e Michelle King por "End"', 'The Good Wife', 33, 2, 0, 0, '2016-08-12 11:39:10', '2016-08-12 11:39:10'),
(175, 'Sam Esmail por "eps1.0_hellofriend.mov (Pilot)"', 'Mr. Robot', 33, 2, 0, 0, '2016-08-12 11:39:10', '2016-08-12 11:39:10'),
(176, 'Marti Noxon e Sarah Gertrude Shapiro por "Return"', 'UnREAL', 33, 2, 0, 0, '2016-08-12 11:39:10', '2016-08-12 11:39:10'),
(177, 'Black-ish', '', 34, 2, 0, 0, '2016-08-12 11:44:10', '2016-08-12 11:44:10'),
(178, 'Master of None', '', 34, 2, 0, 0, '2016-08-12 11:39:49', '2016-08-12 11:39:49'),
(179, 'Modern Family', '', 34, 2, 0, 0, '2016-08-12 11:45:11', '2016-08-12 11:45:11'),
(180, 'Silicon Valley', '', 34, 2, 0, 0, '2016-08-12 11:45:11', '2016-08-12 11:45:11'),
(181, 'Transparent', '', 34, 2, 0, 0, '2016-08-12 11:45:11', '2016-08-12 11:45:11'),
(182, 'Unbreakable Kimmy Schmidt', '', 34, 2, 0, 0, '2016-08-12 11:45:11', '2016-08-12 11:45:11'),
(183, 'Veep', '', 34, 2, 0, 0, '2016-08-12 11:45:11', '2016-08-12 11:45:11'),
(184, 'Julia Louis-Dreyfus', 'Veep', 35, 2, 0, 0, '2016-08-12 11:46:45', '2016-08-12 11:46:45'),
(185, 'Amy Schumer', 'Inside Amy Schumer', 35, 2, 0, 0, '2016-08-12 11:46:45', '2016-08-12 11:46:45'),
(186, 'Lily Tomlin', 'Grace And Frankie', 35, 2, 0, 0, '2016-08-12 11:46:45', '2016-08-12 11:46:45'),
(187, 'Ellie Kemper', 'Unbreakable Kimmy Schmidt', 35, 2, 0, 0, '2016-08-12 11:46:45', '2016-08-12 11:46:45'),
(188, 'Tracee Ellis Ross', 'Black-ish', 35, 2, 0, 0, '2016-08-12 11:46:45', '2016-08-12 11:46:45'),
(189, 'Laurie Metcalf', 'Getting On', 35, 2, 0, 0, '2016-08-12 11:46:45', '2016-08-12 11:46:45'),
(190, 'Anthony Anderson', 'Black-ish', 36, 2, 0, 0, '2016-08-12 11:48:10', '2016-08-12 11:48:10'),
(191, 'Aziz Ansari', 'Master of None', 36, 2, 0, 0, '2016-08-12 11:48:10', '2016-08-12 11:48:10'),
(192, 'Will Forte', 'The Last Man on Earth', 36, 2, 0, 0, '2016-08-12 11:48:10', '2016-08-12 11:48:10'),
(193, 'William H. Macy', 'Shameless', 36, 2, 0, 0, '2016-08-12 11:48:10', '2016-08-12 11:48:10'),
(194, 'Thomas Middleditch', 'Silicon Valley', 36, 2, 0, 0, '2016-08-12 11:48:10', '2016-08-12 11:48:10'),
(195, 'Jeffrey Tambor', 'Transparent', 36, 2, 0, 0, '2016-08-12 11:48:10', '2016-08-12 11:48:10'),
(196, 'Niecy Nash', 'Getting On', 37, 2, 0, 0, '2016-08-12 11:49:29', '2016-08-12 11:49:29'),
(197, 'Allison Janney', 'Mom', 37, 2, 0, 0, '2016-08-12 11:49:29', '2016-08-12 11:49:29'),
(198, 'Kate McKinnon', 'Saturday Night Live', 37, 2, 0, 0, '2016-08-12 11:49:29', '2016-08-12 11:49:29'),
(199, 'Judith Light', 'Transparent', 37, 2, 0, 0, '2016-08-12 11:49:29', '2016-08-12 11:49:29'),
(200, 'Gaby Hoffmann', 'Transparent', 37, 2, 0, 0, '2016-08-12 11:49:29', '2016-08-12 11:49:29'),
(201, 'Anna Chlumsky', 'Veep', 37, 2, 0, 0, '2016-08-12 11:49:29', '2016-08-12 11:49:29'),
(202, 'Louie Anderson', 'Baskets', 38, 2, 0, 0, '2016-08-12 11:50:57', '2016-08-12 11:50:57'),
(203, 'Andre Braugher', 'Brooklyn Nine-Nine', 38, 2, 0, 0, '2016-08-12 11:50:57', '2016-08-12 11:50:57'),
(204, 'Keegan-Michael Key', 'Key & Peele', 38, 2, 0, 0, '2016-08-12 11:50:57', '2016-08-12 11:50:57'),
(205, 'Ty Burrell', 'Modern Family', 38, 2, 0, 0, '2016-08-12 11:50:57', '2016-08-12 11:50:57'),
(206, 'Tituss Burgess', 'Unbreakable Kimmy Schmidt', 38, 2, 0, 0, '2016-08-12 11:50:57', '2016-08-12 11:50:57'),
(207, 'Tony Hale', 'Veep', 38, 2, 0, 0, '2016-08-12 11:50:57', '2016-08-12 11:50:57'),
(208, 'Matt Walsh', 'Veep', 38, 2, 0, 0, '2016-08-12 11:51:24', '2016-08-12 11:51:24'),
(209, 'Laurie Metcalf', 'The Big Bang Theory', 39, 2, 0, 0, '2016-08-12 11:54:34', '2016-08-12 11:54:34'),
(210, 'Christine Baranski', 'The Big Bang Theory', 39, 2, 0, 0, '2016-08-12 11:54:34', '2016-08-12 11:54:34'),
(211, 'Tina Fey e Amy Poehler', 'Saturday Night Live', 39, 2, 0, 0, '2016-08-12 11:54:34', '2016-08-12 11:54:34'),
(212, 'Melissa McCarthy', 'Saturday Night Live', 39, 2, 0, 0, '2016-08-12 11:54:34', '2016-08-12 11:54:34'),
(213, 'Amy Schumer', 'Saturday Night Live', 39, 2, 0, 0, '2016-08-12 11:54:34', '2016-08-12 11:54:34'),
(214, 'Melora Hardin', 'Transparent', 39, 2, 0, 0, '2016-08-12 11:54:34', '2016-08-12 11:54:34'),
(215, 'Bob Newhart', 'The Big Bang Theory', 40, 2, 0, 0, '2016-08-12 11:56:04', '2016-08-12 11:56:04'),
(216, 'Tracy Morgan', 'Saturday Night Live', 40, 2, 0, 0, '2016-08-12 11:56:04', '2016-08-12 11:56:04'),
(217, 'Larry David', 'Saturday Night Live', 40, 2, 0, 0, '2016-08-12 11:56:04', '2016-08-12 11:56:04'),
(218, 'Bradley Whitford', 'Transparent', 40, 2, 0, 0, '2016-08-12 11:56:04', '2016-08-12 11:56:04'),
(219, 'Martin Mull', 'Veep', 40, 2, 0, 0, '2016-08-12 11:56:04', '2016-08-12 11:56:04'),
(220, 'Peter Scolari', 'Girls', 40, 2, 0, 0, '2016-08-12 11:56:04', '2016-08-12 11:56:04'),
(221, 'Aziz Ansari por "Parents"', 'Master of None', 41, 2, 0, 0, '2016-08-12 11:57:46', '2016-08-12 11:57:46'),
(222, 'Alec Berg por "Daily Active Users"', 'Silicon Valley', 41, 2, 0, 0, '2016-08-12 11:57:46', '2016-08-12 11:57:46'),
(223, 'Mike Judge por "Founder Friendly"', 'Silicon Valley', 41, 2, 0, 0, '2016-08-12 11:57:46', '2016-08-12 11:57:46'),
(224, 'Jill Soloway por "Man On The Land"', 'Transparent', 41, 2, 0, 0, '2016-08-12 11:57:46', '2016-08-12 11:57:46'),
(225, 'Dave Mandel por "Kissing Your Sister"', 'Veep', 41, 2, 0, 0, '2016-08-12 11:57:46', '2016-08-12 11:57:46'),
(226, 'Chris Addison por "Morning After"', 'Veep', 41, 2, 0, 0, '2016-08-12 11:57:46', '2016-08-12 11:57:46'),
(227, 'Dale Stern por "Mother"', 'Veep', 41, 2, 0, 0, '2016-08-12 11:58:09', '2016-08-12 11:58:09'),
(228, 'Rob Delaney e Sharon Horgan por Episódio 1', 'Catastrophe', 42, 2, 0, 0, '2016-08-12 11:59:53', '2016-08-12 11:59:53'),
(229, 'Aziz Ansari e Alan Yang por "Parents"', 'Master of None', 42, 2, 0, 0, '2016-08-12 11:59:53', '2016-08-12 11:59:53'),
(230, 'Dan O''Keef por "Founder Friendly"', 'Silicon Valley', 42, 2, 0, 0, '2016-08-12 11:59:53', '2016-08-12 11:59:53'),
(231, 'Alec Berg por "The Uptick"', 'Silicon Valley', 42, 2, 0, 0, '2016-08-12 11:59:53', '2016-08-12 11:59:53'),
(232, 'David Mandel por "Morning After"', 'Veep', 42, 2, 0, 0, '2016-08-12 11:59:53', '2016-08-12 11:59:53'),
(233, 'Alex Gregory e Peter Huyck por "Mother"', 'Veep', 42, 2, 0, 0, '2016-08-12 11:59:53', '2016-08-12 11:59:53'),
(234, 'American Crime', '', 43, 2, 0, 0, '2016-08-12 12:02:34', '2016-08-12 12:02:34'),
(235, 'Fargo', '', 43, 2, 0, 0, '2016-08-12 12:02:34', '2016-08-12 12:02:34'),
(236, 'The Night Manager', '', 43, 2, 0, 0, '2016-08-12 12:02:34', '2016-08-12 12:02:34'),
(237, 'The People v. O.J. Simpson: American Crime Story', '', 43, 2, 0, 0, '2016-08-12 12:02:34', '2016-08-12 12:02:34'),
(238, 'Roots', '', 43, 2, 0, 0, '2016-08-12 12:02:34', '2016-08-12 12:02:34'),
(239, 'All The Way', '', 44, 2, 0, 0, '2016-08-12 12:03:14', '2016-08-12 12:03:14'),
(240, 'Confirmation', '', 44, 2, 0, 0, '2016-08-12 12:03:14', '2016-08-12 12:03:14'),
(241, 'Luther', '', 44, 2, 0, 0, '2016-08-12 12:03:14', '2016-08-12 12:03:14'),
(242, 'Sherlock: The Abominable Bride', '', 44, 2, 0, 0, '2016-08-12 12:03:14', '2016-08-12 12:03:14'),
(243, 'A Very Murray Christmas', '', 44, 2, 0, 0, '2016-08-12 12:03:14', '2016-08-12 12:03:14'),
(244, 'Sarah Paulson', 'The People v. O.J. Simpson: American Crime Story', 45, 2, 0, 0, '2016-08-12 12:04:46', '2016-08-12 12:04:46'),
(245, 'Kerry Washington', 'Confirmation', 45, 2, 0, 0, '2016-08-12 12:04:46', '2016-08-12 12:04:46'),
(246, 'Kirsten Dunst', 'Fargo', 45, 2, 0, 0, '2016-08-12 12:04:46', '2016-08-12 12:04:46'),
(247, 'Felicity Huffman', 'American Crime', 45, 2, 0, 0, '2016-08-12 12:04:46', '2016-08-12 12:04:46'),
(248, 'Audra McDonald', 'Lady Day at Emerson''s Bar & Grill', 45, 2, 0, 0, '2016-08-12 12:04:46', '2016-08-12 12:04:46'),
(249, 'Lili Taylor', 'American Crime', 45, 2, 0, 0, '2016-08-12 12:04:46', '2016-08-12 12:04:46'),
(250, 'Bryan Cranston', 'All The Way', 46, 2, 0, 0, '2016-08-12 12:07:13', '2016-08-12 12:07:13'),
(251, 'Benedict Cumberbatch', 'Sherlock: The Abominable Bride', 46, 2, 0, 0, '2016-08-12 12:07:13', '2016-08-12 12:07:13'),
(252, 'Idris Elba', 'Luther', 46, 2, 0, 0, '2016-08-12 12:07:13', '2016-08-12 12:07:13'),
(253, 'Cuba Gooding Jr.', 'The People v. O.J. Simpson: American Crime Story', 46, 2, 0, 0, '2016-08-12 12:07:13', '2016-08-12 12:07:13'),
(254, 'Tom Hiddleston', 'The Night Manager', 46, 2, 0, 0, '2016-08-12 12:07:13', '2016-08-12 12:07:13'),
(255, 'Courtney B. Vance', 'The People v. O.J. Simpson: American Crime Story', 46, 2, 0, 0, '2016-08-12 12:07:13', '2016-08-12 12:07:13'),
(256, 'Melissa Leo', 'All The Way', 47, 2, 0, 0, '2016-08-12 12:09:39', '2016-08-12 12:09:39'),
(257, 'Regina King', 'American Crime', 47, 2, 0, 0, '2016-08-12 12:09:39', '2016-08-12 12:09:39'),
(258, 'Sarah Paulson', 'American Horror Story: Hotel', 47, 2, 0, 0, '2016-08-12 12:09:39', '2016-08-12 12:09:39'),
(259, 'Kathy Bates', 'American Horror Story: Hotel', 47, 2, 0, 0, '2016-08-12 12:09:39', '2016-08-12 12:09:39'),
(260, 'Jean Smart', 'Fargo', 47, 2, 0, 0, '2016-08-12 12:09:39', '2016-08-12 12:09:39'),
(261, 'Olivia Colman', 'The Night Manager', 47, 2, 0, 0, '2016-08-12 12:09:39', '2016-08-12 12:09:39'),
(262, 'Jesse Plemons', 'Fargo', 48, 2, 0, 0, '2016-08-12 12:10:52', '2016-08-12 12:10:52'),
(263, 'Bokeem Woodbine', 'Fargo', 48, 2, 0, 0, '2016-08-12 12:10:52', '2016-08-12 12:10:52'),
(264, 'Hugh Laurie', 'The Night Manager', 48, 2, 0, 0, '2016-08-12 12:10:52', '2016-08-12 12:10:52'),
(265, 'Sterling K. Brown', 'The People v. O.J. Simpson: American Crime Story', 48, 2, 0, 0, '2016-08-12 12:10:52', '2016-08-12 12:10:52'),
(266, 'David Schwimmer', 'The People v. O.J. Simpson: American Crime Story', 48, 2, 0, 0, '2016-08-12 12:10:52', '2016-08-12 12:10:52'),
(267, 'John Travolta', 'The People v. O.J. Simpson: American Crime Story', 48, 2, 0, 0, '2016-08-12 12:10:52', '2016-08-12 12:10:52'),
(268, 'Bob DeLaurentis por "Loplop"', 'Fargo', 49, 2, 0, 0, '2016-08-12 12:12:55', '2016-08-12 12:12:55'),
(269, 'Noah Hawley por "Palindrome"', 'Fargo', 49, 2, 0, 0, '2016-08-12 12:12:55', '2016-08-12 12:12:55'),
(270, 'David Farr por The Night Manager', '', 49, 2, 0, 0, '2016-08-12 12:12:55', '2016-08-12 12:12:55'),
(271, 'Scott Alexander e Larry Karaszewski por "From The Ashes Of Tragedy"', 'The People v. O.J. Simpson: American Crime Story', 49, 2, 0, 0, '2016-08-12 12:12:55', '2016-08-12 12:12:55'),
(272, 'D.V. DeVincentis por "Marcia, Marcia, Marcia"', 'The People v. O.J. Simpson: American Crime Story', 49, 2, 0, 0, '2016-08-12 12:12:55', '2016-08-12 12:12:55'),
(273, 'Joe Robert Cole por "The Race Card"', 'The People v. O.J. Simpson: American Crime Story', 49, 2, 0, 0, '2016-08-12 12:12:55', '2016-08-12 12:12:55'),
(274, 'Jay Roach', 'All The Way', 50, 2, 0, 0, '2016-08-12 12:15:43', '2016-08-12 12:15:43'),
(275, 'Noah Hawley por "Before The Law"', 'Fargo', 50, 2, 0, 0, '2016-08-12 12:15:43', '2016-08-12 12:15:43'),
(276, 'Susanne Bier', 'The Night Manager', 50, 2, 0, 0, '2016-08-12 12:15:43', '2016-08-12 12:15:43'),
(277, 'Ryan Murphy por "From The Ashes Of Tragedy"', 'The People v. O.J. Simpson: American Crime Story', 50, 2, 0, 0, '2016-08-12 12:15:43', '2016-08-12 12:15:43'),
(278, 'Anthony Hemingway por "Manna From Heaven"', 'The People v. O.J. Simpson: American Crime Story', 50, 2, 0, 0, '2016-08-12 12:15:43', '2016-08-12 12:15:43'),
(279, 'John Singleton por "The Race Card"', 'The People v. O.J. Simpson: American Crime Story', 50, 2, 0, 0, '2016-08-12 12:15:43', '2016-08-12 12:15:43'),
(280, 'Archer', '', 9, 2, 0, 0, '2016-08-12 12:19:21', '2016-08-12 12:19:21'),
(281, 'Bob''s Burgers', '', 9, 2, 0, 0, '2016-08-12 12:19:21', '2016-08-12 12:19:21'),
(282, 'Phineas and Ferb Last Day of Summer', '', 9, 2, 0, 0, '2016-08-12 12:19:21', '2016-08-12 12:19:21'),
(283, 'The Simpsons', '', 9, 2, 0, 0, '2016-08-12 12:19:21', '2016-08-12 12:19:21'),
(284, 'South Park', '', 9, 2, 0, 0, '2016-08-12 12:19:21', '2016-08-12 12:19:21'),
(285, 'Seth MacFarlane', 'Uma Família da Pesada', 51, 2, 0, 0, '2016-08-12 12:20:31', '2016-08-12 12:20:31'),
(286, 'Trey Parker', 'South Park', 51, 2, 0, 0, '2016-08-12 12:20:31', '2016-08-12 12:20:31'),
(287, 'Matt Stone', 'South Park', 51, 2, 0, 0, '2016-08-12 12:20:31', '2016-08-12 12:20:31'),
(288, 'Keegan-Michael Key', 'SuperMansion', 51, 2, 0, 0, '2016-08-12 12:20:31', '2016-08-12 12:20:31'),
(289, 'Chris Pine', 'SuperMansion', 51, 2, 0, 0, '2016-08-12 12:20:31', '2016-08-12 12:20:31'),
(290, 'Comedians In Cars Getting Coffee', '', 52, 2, 0, 0, '2016-08-12 12:22:00', '2016-08-12 12:22:00'),
(291, 'Jimmy Kimmel Live', '', 52, 2, 0, 0, '2016-08-12 12:22:00', '2016-08-12 12:22:00'),
(292, 'Last Week Tonight With John Oliver', '', 52, 2, 0, 0, '2016-08-12 12:22:00', '2016-08-12 12:22:00'),
(293, 'The Late Late Show With James Corden', '', 52, 2, 0, 0, '2016-08-12 12:22:00', '2016-08-12 12:22:00'),
(294, 'Real Time With Bill Maher', '', 52, 2, 0, 0, '2016-08-12 12:22:00', '2016-08-12 12:22:00'),
(295, 'The Tonight Show Starring Jimmy Fallon', '', 52, 2, 0, 0, '2016-08-12 12:22:00', '2016-08-12 12:22:00'),
(296, 'Documentary Now!', '', 53, 2, 0, 0, '2016-08-12 12:22:38', '2016-08-12 12:22:38'),
(297, 'Drunk History', '', 53, 2, 0, 0, '2016-08-12 12:22:38', '2016-08-12 12:22:38'),
(298, 'Inside Amy Schumer', '', 53, 2, 0, 0, '2016-08-12 12:22:38', '2016-08-12 12:22:38'),
(299, 'Key & Peele', '', 53, 2, 0, 0, '2016-08-12 12:22:38', '2016-08-12 12:22:38'),
(300, 'Portlandia', '', 53, 2, 0, 0, '2016-08-12 12:22:38', '2016-08-12 12:22:38'),
(301, 'Saturday Night Live', '', 53, 2, 0, 0, '2016-08-12 12:22:38', '2016-08-12 12:22:38'),
(302, 'Adele Live In New York City', '', 54, 2, 0, 0, '2016-08-12 12:25:14', '2016-08-12 12:25:14'),
(303, 'Amy Schumer: Live At The Apollo', '', 54, 2, 0, 0, '2016-08-12 12:25:14', '2016-08-12 12:25:14'),
(304, 'The Kennedy Center Honors', '', 54, 2, 0, 0, '2016-08-12 12:25:14', '2016-08-12 12:25:14'),
(305, 'The Late Late Show Carpool Karaoke Prime Time Special', '', 54, 2, 0, 0, '2016-08-12 12:25:14', '2016-08-12 12:25:14'),
(306, 'Lemonade', '', 54, 2, 0, 0, '2016-08-12 12:25:14', '2016-08-12 12:25:14'),
(307, 'The Amazing Race', '', 55, 2, 0, 0, '2016-08-12 12:28:09', '2016-08-12 12:28:09'),
(308, 'American Ninja Warrior', '', 55, 2, 0, 0, '2016-08-12 12:28:09', '2016-08-12 12:28:09'),
(309, 'Dancing With The Stars', '', 55, 2, 0, 0, '2016-08-12 12:28:09', '2016-08-12 12:28:09'),
(310, 'Project Runway', '', 55, 2, 0, 0, '2016-08-12 12:28:09', '2016-08-12 12:28:09'),
(311, 'Top Chef', '', 55, 2, 0, 0, '2016-08-12 12:28:09', '2016-08-12 12:28:09'),
(312, 'The Voice', '', 55, 2, 0, 0, '2016-08-12 12:28:09', '2016-08-12 12:28:09'),
(313, 'Ryan Seacrest', 'American Idol', 56, 2, 0, 0, '2016-08-12 12:29:18', '2016-08-12 12:29:18'),
(314, 'Tom Bergeron', 'Dancing With The Stars', 56, 2, 0, 0, '2016-08-12 12:29:18', '2016-08-12 12:29:18'),
(315, 'Jane Lynch', 'Hollywood Game Night', 56, 2, 0, 0, '2016-08-12 12:29:18', '2016-08-12 12:29:18'),
(316, 'Steve Harvey', 'Little Big Shots starring Steve Harvey', 56, 2, 0, 0, '2016-08-12 12:29:18', '2016-08-12 12:29:18'),
(317, 'Tim Gunn', 'Project Runway', 56, 2, 0, 0, '2016-08-12 12:29:18', '2016-08-12 12:29:18'),
(318, 'RuPaul Charles', 'RuPaul''s Drag Race', 56, 2, 0, 0, '2016-08-12 12:29:18', '2016-08-12 12:29:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dyego Cruz', 'dyegocruz@gmail.com', '$2y$10$xPg4eYLVT2ItxJIilDnQRe5/XaD4K2EKL80ubqhTA56pIEg1NXVqK', 'E3zRoJQHG5rZKgQU30NYkt4h6PzEJYvXA8Y89FXdGK2uCfiMviJJb4jsaLrO', '2016-08-10 21:06:21', '2016-08-10 21:06:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `votacoes`
--

CREATE TABLE IF NOT EXISTS `votacoes` (
  `votacao_id` int(11) NOT NULL,
  `votacao_usuario` varchar(255) NOT NULL,
  `votacao_email` varchar(255) NOT NULL,
  `evento_id` int(11) NOT NULL,
  `indicados_serial` text NOT NULL,
  `newsletter` int(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `votacoes`
--

INSERT INTO `votacoes` (`votacao_id`, `votacao_usuario`, `votacao_email`, `evento_id`, `indicados_serial`, `newsletter`, `updated_at`, `created_at`) VALUES
(17, 'Pedro', 'pedrodefarias@outlook.com', 1, 'a:18:{i:1;s:1:"2";i:2;s:2:"12";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"31";i:8;s:2:"41";i:9;s:2:"45";i:11;s:2:"57";i:15;s:2:"77";i:16;s:2:"81";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"95";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"117";}', 1, '2016-01-25 13:01:50', '2016-01-25 05:40:45'),
(18, 'joel barbosa', 'joelbarbosa.ads@gmail.com', 1, 'a:7:{i:1;s:1:"2";i:2;s:2:"12";i:4;s:2:"20";i:5;s:2:"24";i:9;s:2:"45";i:17;s:2:"88";i:21;s:3:"106";}', 1, '2016-01-25 13:01:54', '2016-01-25 13:54:34'),
(19, 'Elizabete', 'betty.bel.viana@hotmail.com', 1, 'a:24:{i:1;s:1:"1";i:2;s:2:"11";i:3;s:2:"14";i:4;s:2:"20";i:5;s:2:"26";i:6;s:2:"31";i:7;s:2:"36";i:8;s:2:"43";i:9;s:2:"45";i:10;s:2:"52";i:11;s:2:"57";i:12;s:2:"62";i:13;s:2:"66";i:14;s:2:"71";i:15;s:2:"78";i:16;s:2:"80";i:17;s:2:"87";i:18;s:2:"90";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"102";i:22;s:3:"108";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-01-25 13:01:58', '2016-01-25 14:16:37'),
(20, 'Marcos', 'marcaum54@gmail.com', 1, 'a:23:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"18";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"32";i:7;s:2:"38";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"52";i:11;s:2:"56";i:12;s:2:"63";i:13;s:2:"66";i:14;s:2:"72";i:15;s:2:"75";i:16;s:2:"81";i:17;s:2:"86";i:18;s:2:"89";i:19;s:2:"92";i:20;s:2:"97";i:21;s:3:"103";i:22;s:3:"110";i:23;s:3:"114";}', 1, '2016-01-25 13:02:01', '2016-01-25 14:23:22'),
(22, 'Caio Jorge Martins Da Silva', 'caio.silva458@gmail.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:2:"12";i:3;s:2:"18";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"35";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"56";i:12;s:2:"63";i:13;s:2:"64";i:14;s:2:"71";i:15;s:2:"77";i:16;s:2:"80";i:17;s:2:"86";i:18;s:2:"89";i:19;s:2:"93";i:20;s:3:"101";i:21;s:3:"102";i:22;s:3:"109";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-01-25 15:15:03', '2016-01-25 15:15:03'),
(23, 'Israel Bruno', 'israelbruno@outlook.com', 1, 'a:20:{i:1;s:1:"8";i:2;s:2:"12";i:3;s:2:"18";i:4;s:2:"20";i:5;s:2:"25";i:6;s:2:"33";i:7;s:2:"36";i:8;s:2:"42";i:9;s:2:"45";i:13;s:2:"66";i:14;s:2:"71";i:15;s:2:"74";i:16;s:2:"80";i:17;s:2:"86";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"102";i:22;s:3:"109";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-01-25 13:28:55', '2016-01-25 13:28:55'),
(24, 'victória duarte', 'victoriaduarte.s@gmail.com', 1, 'a:24:{i:1;s:1:"1";i:2;s:2:"12";i:3;s:2:"15";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"34";i:8;s:2:"43";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"54";i:12;s:2:"62";i:13;s:2:"65";i:14;s:2:"73";i:15;s:2:"77";i:16;s:2:"80";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"121";}', 1, '2016-01-25 15:14:39', '2016-01-25 15:14:39'),
(25, 'Gabriel Teixeira de Almeida Fontenele', 'gabrielfontenele15@gmail.com', 1, 'a:16:{i:1;s:1:"8";i:2;s:2:"12";i:3;s:2:"18";i:4;s:2:"20";i:5;s:2:"24";i:6;s:2:"32";i:7;s:2:"35";i:9;s:2:"47";i:13;s:2:"64";i:16;s:2:"80";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"102";i:23;s:3:"116";}', 1, '2016-01-26 12:57:23', '2016-01-26 12:57:23'),
(26, 'Renato Holanda', 'renatoholanda@live.com', 1, 'a:6:{i:9;s:2:"45";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"102";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-01-26 14:32:48', '2016-01-26 14:32:48'),
(27, 'Mariana ', 'mari-gonca-pinhe@live.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"18";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"36";i:8;s:2:"42";i:9;s:2:"47";i:10;s:2:"53";i:11;s:2:"54";i:12;s:2:"61";i:13;s:2:"64";i:14;s:2:"73";i:15;s:2:"77";i:16;s:2:"81";i:17;s:2:"88";i:18;s:2:"91";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"104";i:22;s:3:"111";i:23;s:3:"114";i:24;s:3:"121";}', 1, '2016-01-26 15:15:16', '2016-01-26 15:15:16'),
(28, 'Carlos Tourinho', 'carlos.tourinho@gmail.com', 1, 'a:23:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"14";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"31";i:7;s:2:"35";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"52";i:11;s:2:"55";i:12;s:2:"62";i:13;s:2:"64";i:14;s:2:"69";i:15;s:2:"78";i:16;s:2:"80";i:17;s:2:"87";i:19;s:2:"94";i:20;s:2:"98";i:21;s:3:"105";i:22;s:3:"109";i:23;s:3:"115";i:24;s:3:"118";}', 1, '2016-01-26 18:54:06', '2016-01-26 18:54:06'),
(29, 'Lucas', 'lucas.brianstorm@gmail.com', 1, 'a:22:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"14";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"32";i:7;s:2:"35";i:8;s:2:"42";i:9;s:2:"48";i:11;s:2:"57";i:12;s:2:"62";i:14;s:2:"71";i:15;s:2:"78";i:16;s:2:"80";i:17;s:2:"84";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"114";i:24;s:3:"118";}', 0, '2016-01-26 19:32:41', '2016-01-26 19:32:41'),
(30, 'Thiago brito', 'thiagoramonb@gmail.com', 1, 'a:24:{i:1;s:1:"1";i:2;s:2:"12";i:3;s:2:"15";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"32";i:7;s:2:"35";i:8;s:2:"42";i:9;s:2:"47";i:10;s:2:"49";i:11;s:2:"58";i:12;s:2:"61";i:13;s:2:"67";i:14;s:2:"73";i:15;s:2:"76";i:16;s:2:"80";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"121";}', 1, '2016-01-26 19:56:40', '2016-01-26 19:56:40'),
(31, 'Lucas Clementino', 'lucas_clementino30@hotmail.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"14";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"37";i:8;s:2:"41";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"54";i:12;s:2:"59";i:13;s:2:"66";i:14;s:2:"70";i:15;s:2:"77";i:16;s:2:"83";i:17;s:2:"85";i:18;s:2:"91";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"118";}', 1, '2016-01-26 21:13:13', '2016-01-26 21:13:13'),
(32, 'Abraão Valinhas Neto', 'abv12@outlook.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"14";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"31";i:7;s:2:"35";i:8;s:2:"39";i:9;s:2:"45";i:10;s:2:"49";i:11;s:2:"58";i:12;s:2:"61";i:13;s:2:"65";i:14;s:2:"69";i:15;s:2:"77";i:16;s:2:"80";i:17;s:2:"87";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"102";i:22;s:3:"108";i:23;s:3:"113";i:24;s:3:"121";}', 1, '2016-01-27 08:41:05', '2016-01-27 08:41:05'),
(33, 'Alexandre Miller', 'alexandre.rellim@gmail.com', 1, 'a:23:{i:1;s:1:"2";i:2;s:2:"12";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"35";i:8;s:2:"42";i:9;s:2:"47";i:10;s:2:"53";i:11;s:2:"56";i:12;s:2:"63";i:13;s:2:"65";i:14;s:2:"73";i:15;s:2:"77";i:16;s:2:"83";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"96";i:20;s:2:"99";i:21;s:3:"105";i:22;s:3:"111";i:23;s:3:"113";i:24;s:3:"121";}', 1, '2016-01-27 10:26:17', '2016-01-27 10:26:17'),
(34, 'Julio Macoggi', 'juliomacoggi@globo.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:2:"12";i:3;s:2:"17";i:4;s:2:"19";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"38";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"52";i:11;s:2:"57";i:12;s:2:"62";i:13;s:2:"66";i:14;s:2:"72";i:15;s:2:"77";i:16;s:2:"80";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"102";i:22;s:3:"109";i:23;s:3:"112";i:24;s:3:"118";}', 1, '2016-01-27 17:19:19', '2016-01-27 17:19:19'),
(35, 'Ricardo', 'missionhill2004@hotmail.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"18";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"33";i:7;s:2:"37";i:8;s:2:"39";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"55";i:12;s:2:"60";i:13;s:2:"65";i:14;s:2:"69";i:15;s:2:"77";i:16;s:2:"81";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"110";i:23;s:3:"113";i:24;s:3:"118";}', 0, '2016-01-29 00:09:14', '2016-01-29 00:09:14'),
(36, 'luide', 'luidefelix@gmail.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:2:"12";i:3;s:2:"18";i:4;s:2:"21";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"34";i:8;s:2:"42";i:9;s:2:"47";i:10;s:2:"50";i:11;s:2:"56";i:12;s:2:"62";i:13;s:2:"65";i:14;s:2:"69";i:15;s:2:"77";i:16;s:2:"79";i:17;s:2:"86";i:18;s:2:"91";i:19;s:2:"93";i:20;s:2:"97";i:21;s:3:"104";i:22;s:3:"108";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-01-29 15:26:39', '2016-01-29 15:26:39'),
(37, 'Caio Túlio Costa', 'caiotuliocosta3@gmail.com', 1, 'a:20:{i:1;s:1:"1";i:2;s:2:"12";i:3;s:2:"14";i:4;s:2:"23";i:5;s:2:"28";i:6;s:2:"31";i:7;s:2:"34";i:8;s:2:"42";i:9;s:2:"48";i:11;s:2:"58";i:14;s:2:"73";i:16;s:2:"80";i:17;s:2:"88";i:18;s:2:"90";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"118";}', 0, '2016-01-29 18:18:53', '2016-01-29 18:18:53'),
(38, 'Rui Gomes', 'ruigomes422@hotmail.com', 1, 'a:21:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"15";i:4;s:2:"23";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"34";i:8;s:2:"42";i:9;s:2:"45";i:12;s:2:"62";i:13;s:2:"64";i:15;s:2:"78";i:16;s:2:"83";i:17;s:2:"87";i:18;s:2:"91";i:19;s:2:"94";i:20;s:3:"101";i:21;s:3:"103";i:22;s:3:"107";i:23;s:3:"113";i:24;s:3:"118";}', 1, '2016-02-13 11:59:52', '2016-02-13 11:59:52'),
(39, 'Lívia Rocha ', 'liviarocha2908@gmail.com', 1, 'a:18:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"14";i:4;s:2:"22";i:5;s:2:"25";i:6;s:2:"31";i:8;s:2:"41";i:9;s:2:"45";i:11;s:2:"54";i:15;s:2:"74";i:16;s:2:"80";i:17;s:2:"87";i:18;s:2:"91";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"102";i:22;s:3:"110";i:24;s:3:"121";}', 1, '2016-02-13 21:31:55', '2016-02-13 21:31:55'),
(40, 'Dyego Cruz', 'dyegocruz@gmail.com', 1, 'a:23:{i:1;s:1:"1";i:2;s:2:"12";i:3;s:2:"15";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"32";i:7;s:2:"35";i:8;s:2:"43";i:9;s:2:"45";i:10;s:2:"49";i:11;s:2:"54";i:13;s:2:"65";i:14;s:2:"69";i:15;s:2:"78";i:16;s:2:"80";i:17;s:2:"87";i:18;s:2:"91";i:19;s:2:"96";i:20;s:2:"99";i:21;s:3:"105";i:22;s:3:"109";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-02-28 20:51:04', '2016-02-28 20:46:16'),
(42, 'Wenderlâny ', 'wenderlany@hotmail.com', 1, 'a:24:{i:1;s:1:"8";i:2;s:1:"9";i:3;s:2:"18";i:4;s:2:"23";i:5;s:2:"28";i:6;s:2:"31";i:7;s:2:"34";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"56";i:12;s:2:"60";i:13;s:2:"65";i:14;s:2:"73";i:15;s:2:"77";i:16;s:2:"81";i:17;s:2:"84";i:18;s:2:"91";i:19;s:2:"94";i:20;s:2:"99";i:21;s:3:"104";i:22;s:3:"110";i:23;s:3:"114";i:24;s:3:"121";}', 0, '2016-02-19 17:32:35', '2016-02-19 17:32:35'),
(43, 'Pedro Coelho', 'pedrocoelhomedeiros@hotmail.com', 1, 'a:24:{i:1;s:1:"2";i:2;s:1:"9";i:3;s:2:"15";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"33";i:7;s:2:"36";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"57";i:12;s:2:"62";i:13;s:2:"65";i:14;s:2:"71";i:15;s:2:"77";i:16;s:2:"83";i:17;s:2:"87";i:18;s:2:"90";i:19;s:2:"93";i:20;s:3:"101";i:21;s:3:"103";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"118";}', 0, '2016-02-23 04:50:01', '2016-02-23 04:50:01'),
(44, 'Lívia Priscilla', 'liviapriscilladfa@yahoo.com.br', 1, 'a:18:{i:1;s:1:"4";i:2;s:2:"10";i:3;s:2:"14";i:4;s:2:"20";i:5;s:2:"26";i:6;s:2:"31";i:7;s:2:"37";i:8;s:2:"41";i:9;s:2:"45";i:16;s:2:"81";i:17;s:2:"87";i:18;s:2:"91";i:19;s:2:"95";i:20;s:3:"100";i:21;s:3:"106";i:22;s:3:"108";i:23;s:3:"115";i:24;s:3:"117";}', 0, '2016-02-24 01:47:27', '2016-02-24 01:47:27'),
(45, 'Carlos', 'cacostta@gmail.com', 1, 'a:23:{i:1;s:1:"2";i:2;s:2:"12";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"30";i:7;s:2:"37";i:8;s:2:"43";i:9;s:2:"47";i:10;s:2:"52";i:11;s:2:"54";i:12;s:2:"61";i:13;s:2:"64";i:14;s:2:"73";i:15;s:2:"76";i:16;s:2:"81";i:17;s:2:"87";i:18;s:2:"90";i:19;s:2:"93";i:20;s:3:"100";i:21;s:3:"102";i:22;s:3:"110";i:23;s:3:"113";i:24;s:3:"118";}', 0, '2016-02-24 01:49:29', '2016-02-24 01:49:29'),
(46, 'Bruno Trajano', 'brunostore@hotmail.com', 1, 'a:24:{i:1;s:1:"1";i:2;s:2:"12";i:3;s:2:"15";i:4;s:2:"23";i:5;s:2:"28";i:6;s:2:"29";i:7;s:2:"36";i:8;s:2:"42";i:9;s:2:"45";i:10;s:2:"53";i:11;s:2:"54";i:12;s:2:"63";i:13;s:2:"66";i:14;s:2:"69";i:15;s:2:"77";i:16;s:2:"80";i:17;s:2:"88";i:18;s:2:"91";i:19;s:2:"93";i:20;s:2:"98";i:21;s:3:"105";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"121";}', 1, '2016-02-28 18:23:14', '2016-02-28 18:23:14'),
(48, 'Mariana Nunes Fernandes', 'marynfernandes@gmail.com', 1, 'a:24:{i:1;s:1:"1";i:2;s:2:"12";i:3;s:2:"15";i:4;s:2:"20";i:5;s:2:"28";i:6;s:2:"32";i:7;s:2:"35";i:8;s:2:"43";i:9;s:2:"45";i:10;s:2:"49";i:11;s:2:"57";i:12;s:2:"62";i:13;s:2:"65";i:14;s:2:"69";i:15;s:2:"78";i:16;s:2:"80";i:17;s:2:"87";i:18;s:2:"91";i:19;s:2:"96";i:20;s:3:"101";i:21;s:3:"105";i:22;s:3:"109";i:23;s:3:"116";i:24;s:3:"121";}', 1, '2016-02-28 20:46:44', '2016-02-28 20:46:44'),
(49, 'Emanoel', 'emanoel.demedeiros@gmail.com', 1, 'a:20:{i:1;s:1:"2";i:2;s:2:"12";i:3;s:2:"15";i:4;s:2:"20";i:5;s:2:"25";i:6;s:2:"29";i:7;s:2:"35";i:8;s:2:"39";i:9;s:2:"45";i:12;s:2:"62";i:15;s:2:"77";i:16;s:2:"83";i:17;s:2:"87";i:18;s:2:"90";i:19;s:2:"93";i:20;s:3:"101";i:21;s:3:"104";i:22;s:3:"109";i:23;s:3:"113";i:24;s:3:"118";}', 0, '2016-02-28 21:06:43', '2016-02-28 21:06:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`categoria_id`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`evento_id`), ADD UNIQUE KEY `evento_slug` (`evento_slug`);

--
-- Indexes for table `indicados`
--
ALTER TABLE `indicados`
  ADD PRIMARY KEY (`indicado_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `votacoes`
--
ALTER TABLE `votacoes`
  ADD PRIMARY KEY (`votacao_id`,`evento_id`,`votacao_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `categoria_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `evento_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `indicados`
--
ALTER TABLE `indicados`
  MODIFY `indicado_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=319;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `votacoes`
--
ALTER TABLE `votacoes`
  MODIFY `votacao_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
