@extends('layouts.admin.default')

@section('content')


    <div class="page-title">
        <span class="title">Eventos</span>
        <div class="description">Eventos de premiações cinematográficas e da TV.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">                
                <div class="card-body">
                	<a href="{{route('admin.eventos.create')}}">
                		<button type="button" class="btn btn-success">Adicionar</button>
                	</a>
                    <table class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th> 
								<th>Nome</th>
								<th>Ano</th> 
								<th>Data</th> 
								<th>Encerrado</th>
								<th>Ação</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th> 
								<th>Nome</th> 
								<th>Ano</th> 
								<th>Data</th> 
								<th>Encerrado</th>
								<th>Ação</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($eventos as $evento)			    
								<tr>
									<th scope="row">{{$evento->evento_id}}</th>
									<td>{{$evento->evento_nome}}</td>
									<td>{{$evento->evento_ano}}</td>
									<td>{{$evento->evento_data}}</td>
									<td>{{returnStatusSimNao($evento->evento_encerrado)}}</td>
									<td>
										<a href="{{route('admin.eventos.edit',['id'=>$evento->evento_id])}}"><button type="button" class="btn btn-info">Editar</button></a>
                                        <!--{!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['admin.eventos.destroy', $evento->id]
                                        ]) !!}
                                            {!! Form::submit('Excluir', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}-->
									</td>				
								</tr> 
							@endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$eventos->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection