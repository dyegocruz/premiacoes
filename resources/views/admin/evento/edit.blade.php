@extends('layouts.admin.default')

@section('content')

<div class="page-title">
	<span class="title">Form Evento</span>	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-body">
				
				{{ Form::model($evento, array('route' => array('admin.eventos.update', $evento->evento_id), 'method' => 'PUT')) }}
				
					<div class="sub-title">Nome</div>
					<div>					
						{!! Form::text('evento_nome', null, ['class' => 'form-control']) !!}
					</div>
					<div class="sub-title">Ano</div>
					<div>					
						{!! Form::number('evento_ano', null, ['class' => 'form-control','size'=>'4','max'=>9999,'min'=>1]) !!}
					</div>
					<div class="sub-title">Data do Evento</div>
					<div>					
						{!! Form::date('evento_data', null, ['class' => 'form-control']) !!}
					</div>				
					<div class="checkbox">
		              	<div class="checkbox3 checkbox-danger checkbox-inline checkbox-check  checkbox-circle checkbox-light">
		                	{!! Form::checkbox('evento_encerrado',1,null, ['id'=>'checkbox-fa-light-3']) !!}
		                	<label for="checkbox-fa-light-3">Encerrado</label>
		              	</div>
	              	</div>

				{!! Form::submit('Atualizar Evento', ['class' => 'btn btn-success']) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection