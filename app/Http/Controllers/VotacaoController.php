<?php

namespace App\Http\Controllers;

use Mail;
use Validator;
use App\Votacao;
use App\Evento;
use App\Indicado;
use App\Categoria;
use Illuminate\Http\Request;

class VotacaoController extends Controller
{

    /**
     * Store a new blog post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all(); 

        if(isset($inputs['cat'])){
            $indicados_serial = serialize($inputs['cat']);
        }        

        $rules = [
            'votacao_usuario' => 'required|min:3|max:255',
            'votacao_email' => 'required|email',
            'cat' => 'required'
        ];

        $messages = [
            'votacao_usuario.required' => 'Favor preencher o campo com o seu Nome.',
            'votacao_usuario.min' => 'Favor digitar um Nome com pelo menos três letras.',
            'votacao_email.required' => 'Favor preencher o campo com o seu E-mail.',
            'votacao_email.email' => 'Favor digitar um E-mail válido.',
            'cat.required' => 'Favor escolher pelo menos um indicado.',
        ];

        $validator = Validator::make($inputs, $rules,$messages);

        if ($validator->fails()) {            
            return redirect()->back()
                    ->withErrors($validator->errors())
                    ->withInput();
        }
        
        //Verifica se o usuário já votou                
        $checkVotoUsuario = Votacao::checkVotoUsuario($inputs['evento_id'],$inputs['votacao_email']);
                
        if($checkVotoUsuario){
            $request->session()->flash('alert-info', 'Você já fez suas escolhas, veja as mesmas novamente logo abaixo.');
            return redirect()->route('viewvotacao', ['id' => $checkVotoUsuario->votacao_id]);
            //return redirect()->back();
        }else{
            $votacaoModel = new Votacao();
            $votacaoModel->votacao_usuario = $inputs['votacao_usuario'];
            $votacaoModel->votacao_email = $inputs['votacao_email'];
            $votacaoModel->indicados_serial = $indicados_serial;
            $votacaoModel->evento_id = $inputs['evento_id'];
            $votacaoModel->newsletter = isset($inputs['newsletter']) ? $inputs['newsletter'] : 0;
            $votacaoModel->save();

            //monta os dados para o template do e-mail
            $evento = Evento::find($inputs['evento_id']);
            $votacao = Votacao::find($votacaoModel->votacao_id);
            $indicados_serial = unserialize($votacao->indicados_serial);
            $indicados_in = implode(',',$indicados_serial);
            
            $indicados = Indicado::findIndicadosByIndicadoIds($indicados_in);
        
            $data = [
                'evento'=>$evento,
                'indicados'=>$indicados,
                'votacao'=>$votacaoModel
                ];

            Mail::send('votacao.template-mail', $data, function($message) use ($data) {                                
                $message->to($data['votacao']->votacao_email, $data['votacao']->votacao_usuario)->subject($data['evento']->evento_nome.' do Modo Meu');
            });

            $meta = [
                'title' => 'Escolhidos de '.$votacao->votacao_usuario.' dos indicados do '.$evento->evento_nome
            ];            

            $request->session()->flash('alert-success', 'Suas escolhas foram salvas e enviadas para o e-mail informado com sucesso!');
            return redirect()->route('viewvotacao', ['id' => $votacao->votacao_id]);
        }

    }

    public function view($votacao_id){
        $votacao = Votacao::find($votacao_id);
        
        if(count($votacao) == 0){
            $meta = ['title' => 'Votação não encontrada - Premiações Modo Meu'];
            return view('votacao.votacao_nao_encontrada')->with(compact('meta'));
        }

        $evento = Evento::find($votacao->evento_id);
        $indicados_serial = unserialize($votacao->indicados_serial);
        $indicados_in = implode(',',$indicados_serial);
            
        $indicados = Indicado::findIndicadosByIndicadoIds($indicados_in);

        $meta = [
            'title' => 'Escolhidos de '.$votacao->votacao_usuario.' dos indicados do '.$evento->evento_nome,
            'og_image'=>'Oscar-Modo-Meu-2016.png'
        ];

        return view('votacao.view')->with(compact('meta','votacao','evento','indicados'));
    }

    //preparar script para selecionar vencedores automaticamente
    // public function view($votacao_id){
    //     $votacao = Votacao::all();        
    //     echo '<pre>';
    //     //print_r($votacao);
    //     $i = 1;
    //     foreach($votacao as $voto){
    //         //echo $i.'-'.$voto->votacao_usuario.'<br>';
    //         $indicados = unserialize($voto->indicados_serial);
    //         //if($i == 1){
    //             //print_r($indicados);
    //             foreach($indicados as $indicado){
    //                 //$categoria = Categoria::find($)
    //                 $indicadoObj = Indicado::find($indicado);
    //                 $categoria = Categoria::find($indicadoObj->categoria_id);
    //                 $categorias[$categoria->categoria_id] = $categoria;
    //                 @$indicadosCat[$categoria->categoria_id][$indicadoObj->indicado_id] = $indicadoObj;
    //                 @$indicadosRes[$categoria->categoria_id][$indicadoObj->indicado_id] += 1;
    //                 //@$indicadoTot[$indicado] += 1;
    //             }
    //         //}            

    //         $i++;
    //     }

    //     foreach($categorias as $cat){
    //         //echo $cat->categoria_id.' - '.$cat->categoria_nome.'<br>';
    //         echo 'update indicados set indicado_vencedor_oficial = 1 where indicado_id = ;--'.$cat->categoria_nome.'<br>';
    //         foreach($indicadosCat[$cat->categoria_id] as $indCat){
    //             $votos = $indicadosRes[$cat->categoria_id][$indCat->indicado_id];
    //             $vencedor = max($indicadosRes[$cat->categoria_id]);
    //             if($vencedor == $votos){
    //                 //echo '--('.$indicadosRes[$cat->categoria_id][$indCat->indicado_id].' - '.max($indicadosRes[$cat->categoria_id]).')'.$indCat->indicado_nome.'<br>';
    //                 //echo 'update indicados set indicado_vencedor_blog = 1 where indicado_id = '.$indCat->indicado_id.';<br>';      
    //             }                
    //         }
    //         //echo '<br>';
    //     }

    //     //print_r($indicadosRes);
    //     exit;
    // }
    
}