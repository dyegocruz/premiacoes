@extends('layouts.default')

@section('content')

	<div class="row">

		<div class="col s12">
			<div class="icon-block">	            
	            <h2 class="center">{{ $meta['title'] }}</h2>

				@if($evento->evento_encerrado == 0)
		            <div class="col s12 center"> 
		            	<p class="light style-font" >As votações para o {{$evento->evento_nome}} estão encerradas. Em breve logo abaixo estará disponível a lista dos ganhadores.</p>
		        	</div>
				@elseif($evento->evento_encerrado)

					<div class="col s12 center"> 
		            	<p class="light style-font">Abaixo você pode ver os vencedores oficiais e das votações feitas aqui no site.</p>
		        	</div>

					<div class="row">

						@foreach ($categorias as $cat_id => $cat_nome)
							<div class="col s12 m12 l6">
							    <div class="col s12">
							    	<h3>{{ $cat_nome }}</h3>
								</div>
								<div class="collection">
								    @foreach ($indicados_cat[$cat_id] as $indicado)
								    	<div href="#!" class="collection-item @if($indicado->indicado_vencedor_oficial == 1) active @endif">
								    		{{$indicado->indicado_nome}} @if ($indicado->indicado_por) - {{$indicado->indicado_por}} @endif
								    		@if($indicado->indicado_vencedor_blog == 1)
								    			<span class="badge"><i class="small material-icons vencedor-blog">insert_grade</i></span>
											@endif
								    	</div>			
									@endforeach
								</div>
							</div>

						@endforeach
					</div>

		       	@endif
	        </div>
		</div>			
	</div>


@stop