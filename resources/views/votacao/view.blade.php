@extends('layouts.default')

@section('content')

<div class="template-view">

	<h2 class="center">Estas foram as escolhas de {{$votacao->votacao_usuario}} para o {{$evento->evento_nome}} no Modo Meu</h2>

	<div class="row">
		@foreach($indicados as $indicado)
			<div class="box col s12 m6 l6">
			    <div class="col s12">
			    	<h3>{{$indicado['categoria_nome']}}</h3>
				</div>
				<div class="chopped box col s12">
				  <p>{{$indicado['indicado_nome']}} @if ($indicado['indicado_por']) - {{$indicado['indicado_por']}} @endif</p>	
				</div>
			</div>
		@endforeach
	</div>

	<div class="row">
		<div class="col s12 m6 l4 center-align">
			<a class="waves-effect waves-light btn-large" href="{{route('indicados',['id'=>$evento->evento_slug])}}"><i class="material-icons left">grade</i>Participe você também!</a>
		</div>
		<div class="col s12 m6 l8">
			<div class="col s12 m12 l6">
				<h3 class="center">Compartilhe com os seus amigos</h3>
			</div>
			<div class="col s12 m12 l3 valign">
				<div class="center">
					<a href="http://www.facebook.com/sharer.php?u={{route('viewvotacao',['id'=>$votacao->votacao_id])}}" target="_blank">
		                {{ Html::image('resources/assets/images/facebook.png', 'Facebook') }} 
		            </a>
		            <a href="https://twitter.com/intent/tweet?original_referer={{route('viewvotacao',['id'=>$votacao->votacao_id])}}&url={{route('viewvotacao',['id'=>$votacao->votacao_id])}}&via=modomeu" target="_blank">
		                {{ Html::image('resources/assets/images/twitter.png', 'Twitter') }}
		            </a>
		            <a href="https://plus.google.com/share?url={{route('viewvotacao',['id'=>$votacao->votacao_id])}}" target="_blank">
		                {{ Html::image('resources/assets/images/gplus.png', 'G+') }}
		            </a>
				</div>
			</div>	
		</div>
	</div>

</div>

@stop