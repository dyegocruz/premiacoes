<!doctype html>
<html>
<head>
    @include('layouts.admin.head')
</head>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-expand-toggle">
                            <i class="fa fa-bars icon"></i>
                        </button>
                        <ol class="breadcrumb navbar-breadcrumb">
                            <li class="active">Dashboard</li>
                        </ol>
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-th icon"></i>
                        </button>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                        <!--<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-comments-o"></i></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li class="title">
                                    Notification <span class="badge pull-right">0</span>
                                </li>
                                <li class="message">
                                    No new notification
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown danger">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> 4</a>
                            <ul class="dropdown-menu danger  animated fadeInDown">
                                <li class="title">
                                    Notification <span class="badge pull-right">4</span>
                                </li>
                                <li>
                                    <ul class="list-group notifications">
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge">1</span> <i class="fa fa-exclamation-circle icon"></i> new registration
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge success">1</span> <i class="fa fa-check icon"></i> new orders
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge danger">2</span> <i class="fa fa-comments icon"></i> customers messages
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item message">
                                                view all
                                            </li>
                                        </a>
                                    </ul>
                                </li>
                            </ul>
                        </li>-->
                        
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <!--<li><a href="{{ url('/register') }}">Register</a></li>-->
                        @else
                            <li class="dropdown profile">                                
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
                                <ul class="dropdown-menu animated fadeInDown">                                
                                    <li>
                                        <div class="profile-info">
                                            <h4 class="username">{{ Auth::user()->name }}</h4>
                                            <p>{{ Auth::user()->email }}</p>
                                            <div class="btn-group margin-bottom-2x" role="group">                                                
                                                <a href="{{ url('/logout') }}"><button type="button" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</button></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        @endif                                                
                    </ul>
                </div>
            </nav>
            <div class="side-menu sidebar-inverse">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="side-menu-container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <div class="icon fa fa-paper-plane"></div>
                                <div class="title">Flat Admin V.2</div>
                            </a>
                            <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                                <i class="fa fa-times icon"></i>
                            </button>
                        </div>
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="{{route('dashboard')}}">
                                    <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                                </a>
                            </li>
                            <!--<li class="panel panel-default dropdown">
                                <a data-toggle="collapse" href="#dropdown-element">
                                    <span class="icon fa fa-desktop"></span><span class="title">UI Kits</span>
                                </a>                                
                                <div id="dropdown-element" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            
                                        </ul>
                                    </div>
                                </div>
                            </li>-->
                            <li>
                                <a href="{{route('admin.eventos.index')}}">
                                    <span class="icon fa fa-birthday-cake"></span><span class="title">Eventos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin.categorias.index')}}">
                                    <span class="icon fa fa-list"></span><span class="title">Categorias</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin.indicados.index')}}">
                                    <span class="icon fa fa-film"></span><span class="title">Indicados</span>
                                </a>
                            </li>
                            <li>
                                <a href="license.html">
                                    <span class="icon fa fa-thumbs-o-up"></span><span class="title">License</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </div>
            <!-- Main Content -->
            <div class="container-fluid">                                

                <div class="side-body padding-top">

                    @include('messages.errors')
                    @include('messages.alerts')

                    @yield('content')    
                </div>
            </div>
            <footer class="app-footer">
                @include('layouts.admin.footer')                
            </footer>
        </div>
    </div>

    <!-- Javascript Libs -->
    <script type="text/javascript" src="{!! asset('public/assets/js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/Chart.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/bootstrap-switch.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/jquery.matchHeight-min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/jquery.dataTables.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/dataTables.bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/select2.full.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/ace/ace.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/ace/mode-html.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/ace/theme-github.js') !!}"></script>
    <!-- Javascript -->
    <script type="text/javascript" src="{!! asset('public/assets/js/app.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/assets/js/index.js') !!}"></script>    
</body>
</html>