@extends('layouts.admin.default')

@section('content')

    <div class="page-title">
        <span class="title">Indicados</span>
        <div class="description">Indicados por evento e indicado.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">
                	<div class="row">
                        <div class="col-xs-12">
                            {!! Form::open([
                                'route' => 'admin.indicados.search'
                            ]) !!}
                                <input type="text" class="form-control" name="search" />
                                {!! Form::submit('Buscar', ['class' => 'btn btn-success']) !!}
                            {!! Form::close() !!}                                        
                            <a href="{{route('admin.indicados.create')}}">
                                <button type="button" class="btn btn-success">Adicionar</button>
                            </a>
                            <a href="{{route('admin.indicados.createmany')}}">
                                <button type="button" class="btn btn-success">Adicionar Vários</button>
                            </a>
                        </div>   
                    </div>
                    <table class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th> 
								<th>Nome</th>
                                <th>Indicado Por</th>
                                <th>Categoria</th>
                                <th>Evento</th>
								<th>Ação</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th> 
                                <th>Nome</th>
                                <th>Indicado Por</th>
                                <th>Categoria</th>
                                <th>Evento</th>
                                <th>Ação</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($indicados as $indicado)
								<tr>
									<th scope="row">{{$indicado->indicado_id}}</th>
                                    <td>{{$indicado->indicado_nome}}</td>
                                    <td>{{$indicado->indicado_por}}</td>
                                    <td>{{$categoria::find($indicado->categoria_id)->categoria_nome}}</td>
									<td>{{$evento::find($indicado->evento_id)->evento_nome}}</td>
									<td>
										<a href="{{route('admin.indicados.edit',['id'=>$indicado->indicado_id])}}"><button type="button" class="btn btn-info">Editar</button></a>
                                        <!--{!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['admin.indicados.destroy', $indicado->id]
                                        ]) !!}
                                            {!! Form::submit('Excluir', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}-->
									</td>				
								</tr> 
							@endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$indicados->render()}}
                    </div>              
                </div>
            </div>
        </div>
    </div>

@endsection