$(function() {
    $(".navbar-expand-toggle").click(function() {
      $(".app-container").toggleClass("expanded");
      return $(".navbar-expand-toggle").toggleClass("fa-rotate-90");
    });
    return $(".navbar-right-expand-toggle").click(function() {
      $(".navbar-right").toggleClass("expanded");
      return $(".navbar-right-expand-toggle").toggleClass("fa-rotate-90");
    });
});

$(function() {
    return $('select').select2();
});

$(function() {
    return $('.toggle-checkbox').bootstrapSwitch({
      size: "small"
    });
});

$(function() {
    return $('.match-height').matchHeight();
});

$(function() {
    return $('.datatable').DataTable({
      "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>'
    });
});

$(function() {
    return $(".side-menu .nav .dropdown").on('show.bs.collapse', function() {
        return $(".side-menu .nav .dropdown .collapse").collapse('hide');
    });
});

//função responsável por adicionar mais campos de indicados na tela de cadastro de vários indicados por categoria e por evento
$(function() {
    var max_fields      = 6; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row"><div class="col-xs-6"><div><input class="form-control" name="indicado_nome[]" type="text"></div></div><div class="col-xs-5"><div><input class="form-control" name="indicado_por[]" type="text"></div></div><div class="col-xs-1"><a href="#" class="remove_field"><i class="fa fa-remove"></i></a></div></div>'); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    });
});